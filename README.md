Laradock

## Introduce
Docker is an open platform for developing, shipping, and running applications. Docker enables you to separate your applications from your infrastructure so you can deliver software quickly. With Docker, you can manage your infrastructure in the same ways you manage your applications. By taking advantage of Docker’s methodologies for shipping, testing, and deploying code quickly, you can significantly reduce the delay between writing code and running it in production.
___

## Quick Overview

1. Clone Laradock inside your PHP project:

    ```
        git clone ...
    ```

2. Enter the laradock folder and rename env-example to .env.

    ```
        cp env-example .env
    ```

3. Run your containers:

    ```
        docker-compose up -d nginx postgres redis ...
    ```

4. Open your project’s .env file

5. That's it! enjoy :)
