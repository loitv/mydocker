# Enigma Employee Backend API

> Artisan command:

```php
cd /path/to/enigma-employee_backend

php artisan
```
- [Product requirements](https://neo-universe.atlassian.net/wiki/spaces/ENIGMA/pages/85117753/Services)

- [Bitbucket](https://bitbucket.org/nldanang/enigma-employee_backend)

- Basic authentication: `enigma/cie1yiVa`


## Login
**1. URL**

- POST: {URL}/authenticate

**2. Description**

- Login employee

**3. Last modify**

	// TO DO

**4. Parameter**

- company_code:
	+ required
	+ type_code
	+ max:255
- employee_code:
	+ required
	+ type_code
	+ max:255
- employee_password:
	+ length(6, 255)

> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "Login success",
        data: {
            token,
            user_info: {
                id,
                company_id,
                code,
                email,
                name,
                status,
                company: {
                    id,
                    name,
                    name_kana,
                    code
                }
            },
            csrf_token
        }
    }

- 401:
    {
        status_code: 401,
        message: "Your Company ID , Employee ID or Password is incorrect",
        errors: []
    }

- 403:
    {
        status_code: 403,
        message: "Because this account was stopped using so you can not use this service.",
        errors: {
            status: 5
        }
    }

- 500:
    {
        status_code: 500,
        message: "Occur an error while logging.",
        errors: []
    }
```

**6. Code handle**

- Controller - function:
	+ AuthenticateController@login
- Service:
- Repository:

**7. Table get data**

- employees
- companies
- employee_status_stopped

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

## Password
### Email password
**1. URL**

- POST: {URL}/password/email

**2. Description**

- Get link reset password email.
- Notice: This function will send link reset password email for employee who reset password (token, url, company Id, employee Id, employee name).

**3. Last modify**

	// TO DO

**4. Parameter**

- email:
	+ required
	+ email
	+ exists:employees,email
- employee_code:
	+ required
	+ type_code
	+ max:20
	+ exists:employees,code
- company_code:
	+ required
	+ type_code
	+ max:20
	+ company_code_lower:companies,code

> **5. Response**

```php
- 200:
    {
        status: 200,
        message: "Send password reset success"
        data: []
    }


- 422:
    {
        status: 422,
        message: "Send password reset failed"
        errors: {
            "company_code": [
                "The Company code field is required.",
                "The selected Company code is invalid.",
                "The Company code may not be greater than 20 characters.",
                "The Company code should not contain any special characters.",
            ],
            "employee_code": [
                "The Employee code field is required.",
                "The selected Employee code is invalid.",
                "The Employee code may not be greater than 20 characters.",
                "The Employee code should not contain any special characters.",
            ],
            "email": [
                "The Email field is required.",
                "The Email must be a valid email address.",
                "The selected Email is invalid."
            ]
        }
    }

- 500:
    {
        status: 500,
        message: "Send password reset failed"
        errors: []
    }
```

**6. Code handle**

- Controller - function:
	+ PasswordController@getLinkResetPasswordEmail
- Service:
	+ App\Services\Password\GetLinkResetPasswordService
	+ App\Services\Password\CommonService

**7. Table get data**

- employee_password_resets
- employees
- companies

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**

	// TO DO

### Reset password
**1. URL**

- POST: {URL}/password/reset

**2. Description**

- Reset employee password by email.
- Notice: This function will send changed password mail (employee name and url login).

**3. Last modify**

	// TO DO

**4. Parameter**

- company_id:
	+ required
	+ integer
	+ exists:companies,id,deleted_at,NULL
- company_code:
	+ required
	+ type_code
	+ max:20
	+ exists:employees,code
- token:
	+ required
	+ exists:employee_password_resets,token
- employee_id:
	+ required
	+ integer
	+ exists:employees,id,deleted_at,NULL
- employee_code:
	+ required
	+ type_code
	+ max:20
	+ exists:companies,code
- email:
	+ required
	+ email
	+ exists:employee_password_resets,email
- password:
	+ required
	+ require_trim
	+ length(6, 255)
- password_confirmation:
	+ require_same:password

> **5. Response**

```php
- 200:
	{
		status: 200,
		message: "Reset password success"
		data: []
	}

- 422:
	{
		status: 422,
		message: "Reset password failed"
		errors: {
            "company_code": [
                "The selected Company code is invalid."
            ],
            "employee_code": [
                "The selected Employee code is invalid."
            ],
            "company_id": [
                "The Company ID field is required."
            ],
            "token": [
                "The selected Token is invalid."
            ],
            "employee_id": [
                "The employee id field is required."
            ],
            "email": [
                "The Email field is required."
            ],
            "password": [
                "The Password should not contain any full width characters."
            ]
        }

- 500:
	{
		status: 500,
		message: "Reset password failed"
		errors: []
	}
```

**6. Code handle**

- Controller - function:
	+ PasswordController@resetByEmail
- Service:
	+ App\Services\Password\ResetPasswordService
	+ App\Services\Password\CommonService

**7. Table get data**

- employee_password_resets
- employees
- companies

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**

	// TO DO

### Check Url reset password
**1. URL**

- GET: {URL}/password/reset

**2. Description**

- Check url reset password.

**3. Last modify**

	// TO DO

**4. Parameter**

- company_id:
	+ required
	+ integer
	+ exists:companies,id,deleted_at,NULL
- id:
	+ required
	+ integer
	+ exists:employee_password_resets,employee_id,token
	+ exists:employees,id,deleted_at,NULL,company_id
- token:
	+ required
	+ integer

> **5. Response**

```php
- 200:
	{
		status: 200,
		message: ""
		data: {
			is_valid: true   
		}
	}

- 200:
	{
		status: 200,
		message: ""
		data: {
			is_valid: false,
			message: "This URL is invalid."   
		}
	}
```

**6. Code handle**

- Controller - function:
	+ PasswordController@checkUrlReset
- Service:
	+ App\Services\Password\CheckResetPasswordUrlService
	+ App\Services\Password\CommonService

**7. Table get data**

- employee_password_resets
- employees
- companies

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**

	// TO DO

### Forgot password
**1. URL**

POST: {URL}/password/forgot

**2. Description**

- Create employee forgot password.

**3. Last modify**

	// TO DO

**4. Parameter**

- company_code:
	+ required
	+ type_code
	+ max:255
	+ company_code_lower:companies,code
- employee_name:
	+ required
	+ max:255
- employee_code:
	+ required
	+ type_code
	+ exists:employees,code,deleted_at,NULL
	+ max:20
- employee_phone:
	+ required
	+ phone_number
	+ max: 20

> **5. Response**

```php
- 200:
	{
		status: 200,
		message: "Request send successfully."
		data: []
	}

- 422:
	{
		status: 422,
		message: "The info of employee does not match."
		errors: []
	}

500:
	{
		status: 500,
		message: "Occur error when request reset password."
		errors: []
	}
```

**6. Code handle**

- Controller - function:
	+ PasswordController@forgotPassword
- Service:
- Repositories:
	+ App\Repositories\Eloquent\ForgotPasswordRepository

**7. Table get data**

- employee_forgot_passwords
- employees
- companies

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**

	// TO DO

## App
### Version app
**1. URL**

- GET: {URL}/app/version

**2. Description**

- Get newest version.

**3. Last modify**

	// TO DO

**4. Parameter**

> **5. Response**

```php
- 200:
	{
		status: 200,
		message: ""
		data: {
			version
		}
	}
- 500:
	{
		status: 500,
		message: "Occur an error while get newest app version."
		errors: []
	}
```

**6. Code handle**

- Controller - function:
	+ MobileController@getNewestVersion
- Service:
- Repositories:
	+ App\Repositories\Eloquent\EnigmaSettingRepository

**7. Table get data**

- enigma_settings

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**

	// TO DO

### Url app
**1. URL**

- GET: {URL}/app/url

**2. Description**

- Get url follow version for mobile.

**3. Last modify**

	// TO DO

**4. Parameter**

- version: 
	+ nullable

> **5. Response**

```php
- 200:
	{
		status: 200,
		message: ""
		data: {
			url
		}
	}
- 500:
	{
		status: 500,
		message: "Occur an error while get app url."
		errors: []
	}
```

**6. Code handle**

- Controller - function:
	+ MobileController@getApiUrl
- Service:
- Repositories:
	+ App\Repositories\Eloquent\EnigmaAppVersionRepository

**7. Table get data**

- enigma_app_versions

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**

	// TO DO

## Maintenance
**1. URL**

- GET: {URL}/check-maintenance

**2. Description**

- Check maintenance

**3. Last modify**

	// TO DO

**4. Parameter**

> **5. Response**

```php
- 200:
    {
        status: 200,
        message: ""
        data: []
    }
```

**6. Code handle**

- Controller - function:
	+ MaintenanceController@index

**7. Table get data**

	// TO DO

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**

	// TO DO

## Version api
**1. URL**

- GET: {URL}/version

**2. Description**

- API get version.

**3. Last modify**

	// TO DO

**4. Parameter**

> **5. Response**

```php	
{
    message: "Enigma version release"
    data: {
        version: "v2.2.1"
    }
}
```

**6. Code handle**

	// TO DO

**7. Table get data**

	// TO DO

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

## Logout
**1. URL**

- GET: {URL}/logout

**2. Description**

- Employee logout.

**3. Last modify**

	// TO DO

**4. Parameter**

> **5. Response**

```php	
- 200:
    {
        status: 200,
        message: "logout success"
        data: []
    }
```    

**6. Code handle**

- Controller - function:
    + AuthenticateController@logout

**7. Table get data**

	// TO DO

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**

	// TO DO

## Terms using service
### Get terms using service
**1. URL**

- GET: {URL}/employee/agree

**2. Description**

- Get the terms using service page content

**3. Last modify**

	// TO DO

**4. Parameter**

> **5. Response**

```php	
- 200:
    {
        status: 200,
        message: "Get content of terms using service page successfully."
        data: {
            page_content
        }
    }
```

**6. Code handle**

- Controller - function:
    + AuthenticateController@getTermsUsingService

**7. Table get data**

	// TO DO

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**

	// TO DO

### Agree terms using service
**1. URL**

- POST: {URL}/employee/agree

**2. Description**

- Agree with the terms of service

**3. Last modify**

	// TO DO

**4. Parameter**

- agree:
    + required
    + accepted

> **5. Response**

```php	
- 200:
    {
        status: 200,
        message: "Agree with the terms of service."
        data: []
    }

- 403:
    {
        status: 403,
        message: "Permission denied."
        errors: []
    }
- 422:
    {
        status: 422,
        message: "Data validation failed"
        errors: []
    }
- 500:
    {
        status: 500,
        message: "It have a error when agreeing with the terms of service."
        errors: []
    }
```

**6. Code handle**

- Controller - function:
    + AuthenticateController@agreeWithTerms
- Repositories:
    + App\Repositories\Eloquent\EmployeeRepository

**7. Table get data**

- employees
- employee_status_stopped

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**

	// TO DO

## Confirm account
**1. URL**

- POST: {URL}/employee/confirm

**2. Description**

- Confirm email and password

**3. Last modify**

	// TO DO

**4. Parameter**

- email:
    + email
    + max:255
    + nullable
- password:
    + require_trim
    + required
    + length(6, 255)
- password_confirmation:
    + require_same:password

> **5. Response**

```php	
- 200:
    {
        status: 200,
        message: "User information has been registered."
        data: []
    }

- 403:
    {
        status: 403,
        message: "Permission denied."
        errors: []
    }
- 422:
    {
        status: 422,
        message: "Data validation failed"
        errors: []
    }
- 500:
    {
        status: 500,
        message: "It have a error when registering user information."
        errors: []
    }
```

**6. Code handle**

- Controller - function:
    + AuthenticateController@confirmAccount
- Repositories:
    + App\Repositories\Eloquent\EmployeeRepository

**7. Table get data**

- employees
- employee_status_stopped

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**

	// TO DO

## Push setting
### Setting device token
**1. URL**

- POST: {URL}/employee/push-setting

**2. Description**

- Setting device token

**3. Last modify**

	// TO DO

**4. Parameter**

- device_token
- device_type

> **5. Response**

```php	
- 200:
    {
        status: 200,
        message: "Setting employee device token success."
        data: []
    }

- 422:
    {
        status: 422,
        message: "Data validation failed"
        errors: []
    }
- 500:
    {
        status: 500,
        message: "Setting employee device token fail."
        errors: []
    }
```

**6. Code handle**

- Controller - function:
    + PushSettingController@settingDeviceToken
- Repositories:
	+ App\Repositories\Eloquent\PushSettingRepository

**7. Table get data**

- push_settings

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**

	// TO DO

### Update receive notification
**1. URL**

- PUT: {URL}/employee/push-setting

**2. Description**

- Update setting receive notification

**3. Last modify**

	// TO DO

**4. Parameter**

- from_enigma

> **5. Response**

```php	
- 200:
    {
        status: 200,
        message: "Update opt-in setting successfully."
        data: []
    }

- 500:
    {
        status: 500,
        message: "Fail to update opt-in setting."
        errors: []
    }
```

**6. Code handle**

- Controller - function:
    + PushSettingController@updateReceiveNotification
- Repositories:
    + App\Repositories\Eloquent\PushSettingRepository

**7. Table get data**

- push_settings

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**

	// TO DO

### Remove device token
**1. URL**

- DELETE: {URL}/employee/push-setting

**2. Description**

- Remove device token

**3. Last modify**

	// TO DO

**4. Parameter**

> **5. Response**
	
```php	
- 200:
    {
        status: 200,
        message: "Remove employee device token successfully."
        data: []
    }

- 500:
    {
        status: 500,
        message: "Fail to remove employee device token."
        errors: []
    }
```

**6. Code handle**

- Controller - function:
    + PushSettingController@removeDeviceToken
- Repositories:
	+ App\Repositories\Eloquent\PushSettingRepository

**7. Table get data**

- push_settings

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**

	// TO DO

### Get employee setting
**1. URL**

- GET: {URL}/employee/settings

**2. Description**

- Get employee setting

**3. Last modify**

	// TO DO

**4. Parameter**

> **5. Response**

```php	
- 200:
    {
        status: 200,
        message: ""
        data: {
            push_setting
        }
    }

- 500:
    {
        status: 500,
        message: "Occur an error while get employee setting."
        errors: []
    }
```

**6. Code handle**

- Controller - function:
    + CommonApiController@getSetting
- Repositories:
	+ App\Repositories\Eloquent\PushSettingRepository

**7. Table get data**

- push_settings

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**

	// TO DO

## Notification
### Get notifications
**1. URL**

- GET: {URL}/notifications

**2. Description**

- Get list notification

**3. Last modify**

	// TO DO

**4. Parameter**

- per_page: nullable

> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "",
        data: {
            total,
            per_page,
            current_page,
            last_page,
            next_page_url,
            prev_page_url,
            from,
            to,
            items: [
                {
                    id,
                    title,
                    content,
                    send_date,
                    created_at,
                    is_read
                }
            ]
        }
    }
- 500:
    {
        status: 500,
        message: "Occur an error while listing notifications."
        errors: []
    }
```

**6. Code handle**

- Controller - function:
    + NotificationController@index
- Service:
- Repository:
    + App\Repositories\NotificationRepository

**7. Table get data**

- notifications
- notification_receives

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**

	// TO DO

### Read all
**1. URL**

- PUT: {URL}/notifications/read-all

**2. Description**

- Read all notification (update is_read is true)

**3. Last modify**

	// TO DO

**4. Parameter**

> **5. Response**

```php
- 200:
    {
        status: 200,
        message: ""
        data: []
    }
- 500:
    {
        status: 500,
        message: "Occur an error while reading all notification."
        errors: []
    }
```

**6. Code handle**

- Controller - function:
    + NotificationController@readAll
- Service:
- Repository:
    + App\Repositories\NotificationRepository

**7. Table get data**

- notifications
- notification_receives

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**

	// TO DO

### Show
**1. URL**

- GET: {URL}/notifications/{id}

**2. Description**

- Show notification and update is_read is true

**3. Last modify**

	// TO DO

**4. Parameter**

- id: required

> **5. Response**

```php
- 200:
    {
        status: 200,
        message: ""
        data: {
            id,
            title,
            content,
            send_date,
            created_at,
            is_read
        }
    }

- 404:
    {
        status_code: 404,
        message: "Notification is not found.",
        errors: []
    }
- 500:
    {
        status: 500,
        message: "Occur an error while showing notification."
        errors: []
    }
```

**6. Code handle**

- Controller - function:
    + NotificationController@show
- Service:
- Repository:
    + App\Repositories\NotificationRepository

**7. Table get data**

- notifications
- notification_receives

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**

	// TO DO

### Read
**1. URL**

- PUT: {URL}/notifications/{id}/read

**2. Description**

- Read notification

**3. Last modify**

	// TO DO

**4. Parameter**

- id: required

> **5. Response**

```php
- 200:
    {
        status: 200,
        message: ""
        data: {
            is_read:true
        }
    }

- 404:
    {
        status_code: 404,
        message: "Notification is not found.",
        errors: []
    }

- 500:
    {
        status: 500,
        message: "Occur an error while reading notification."
        errors: []
    }
```

**6. Code handle**

- Controller - function:
    + NotificationController@read
- Service:
- Repository:
	+ App\Repositories\NotificationRepository

**7. Table get data**

- notifications
- notification_receives

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**

	// TO DO

## Employee
### Stop using service
**1. URL**

- POST: {URL}/employee/stop-service

**2. Description**

- Stop using service (update status is stop)
- Note: Send notification email for stopped using service

**3. Last modify**

	// TO DO

**4. Parameter**

- password:
    + require_trim
    + required
    + length(6, 255)
    + current_password	

> **5. Response**

```php
- 200:
    {
        status: 200,
        message: "Employee has been stopped using successfully"
        data: []
    }
- 422:
    {
        status: 422,
        message: "Data validation failed"
        errors: []
    }
- 500:
    {
        status: 500,
        message: "Stop employee fail"
        errors: []
    }
```

**6. Code handle**

- Controller - function:
    + EmployeeController@stopUsingService
- Service:
- Repository:
    + App\Repositories\EmployeeRepository
    + App\Repositories\EmployeeStatusStopRepository

**7. Table get data**

- employee_status_stopped
- employees

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**

	// TO DO

### Show
**1. URL**

- GET: {URL}/employee

**2. Description**

- Show employee's infomation

**3. Last modify**

	// TO DO

**4. Parameter**

> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: [],
        data: {
            id,
            company_id,
            code,
            email,
            name,
            status,
            is_getting_announcement,
            company: {
                id,
                name,
                name_kana,
                code
            }
        }
    }
- 500:
    {
        status: 500,
        message: "Employee cannot be showed."
        errors: []
    }
```

**6. Code handle**

- Controller - function:
    + EmployeeController@show
- Service:
- Repository:
    + App\Repositories\EmployeeRepository

**7. Table get data**

- companies
- employees

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

### Update
**1. URL**

- PUT: {URL}/employee

**2. Description**

- Update employee's profile

**3. Last modify**

	// TO DO

**4. Parameter**

- email:
    + email
    + max:255
    + nullable
- is_getting_announcement:
	+ required
	+ boolean
- current_password:
	+ require_trim
	+ required
	+ current_password		

> **5. Response**

```php
- 200:
    {
        status: 200,
        message: "Your profile has been updated successfully."
        data: []
    }
- 403:
    {
        status: 403,
        message: "Permission denied."
        errors: []
    }
- 422:
    {
        status: 422,
        message: "Data validation failed"
        errors: []
    }
- 500:
    {
        status: 500,
        message: "Occur error when updating your profile."
        errors: []
    }
```

**6. Code handle**

- Controller - function:
    + EmployeeController@update
- Service:
- Repository:
    + App\Repositories\EmployeeRepository

**7. Table get data**

- employees

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**

	// TO DO

### Change password
**1. URL**

- PUT: {URL}/employee/password

**2. Description**

- Change employee's password

**3. Last modify**

	// TO DO

**4. Parameter**

- current_password:
    + require_trim
    + required
    + length(6, 255)
    + current_password
- password
	+ require_trim
	+ required
	+ length(6, 255)
	+ different:current_password
- password_confirmation
	+ require_same:password

> **5. Response**

```php
- 200:
    {
        status: 200,
        message: "Your password has changed successfully."
        data: []
    }
- 403:
    {
        status: 403,
        message: "Permission denied."
        errors: []
    }
- 422:
    {
        status: 422,
        message: "Data validation failed"
        errors: []
    }
- 500:
    {
        status: 500,
        message: "Occur an error while changing password."
        errors: []
    }
```

**6. Code handle**

- Controller - function:
    + EmployeeController@changePassword
- Service:
- Repository:
	+ App\Repositories\EmployeeRepository

**7. Table get data**

- employees

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**

	// TO DO

### Notification and announcement unread
**1. URL**

- GET: {URL}/notification-announcement/unreads

**2. Description**

- Count notification and announcement unread

**3. Last modify**

	// TO DO

**4. Parameter**

> **5. Response**

```php
- 200:
    {
        status: 200,
        message: ""
        data: {
            announcement_unread,
            notification_unread,
            total_unread
        }
    }
- 500:
    {
        status: 500,
        message: "Occur an error while counting unread announcement and notification."
        errors: []
    }
```

**6. Code handle**

- Controller - function:
    + AnnouncementController@unread
- Service:
    + App\Services/NotificationService
    + App\Services/AnnouncementService
- Repository:
    + App\Repositories\AnnouncementRepository
    + App\Repositories\NotificationRepository

**7. Table get data**

- notifications
- notification_receives
- enigma_announcements
- announcement_branch
- announcement_receives

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**

	// TO DO

## Announcement
### list
**1. URL**

- GET: {URL}/announcements

**2. Description**

- Get list announcements

**3. Last modify**

	// TO DO

**4. Parameter**

- per_page:nullable
- list_status	

> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message": "",
        data: {
            total,
            per_page,
            current_page,
            last_page,
            next_page_url,
            prev_page_url,
            from,
            to,
            items [
                {
                    id,
                    title,
                    content,
                    start_time_at,
                    is_emergency,
                    type,
                    is_read
                }
            ]
        }
    }
- 500:
    {
        status: 500,
        message: "Occur an error while listing announcements."
        errors: []
    }
```

**6. Code handle**

- Controller - function:
    + AnnouncementController@index
- Service:
    + App\Services/AnnouncementService
- Repository:
    + App\Repositories\AnnouncementRepository

**7. Table get data**

- enigma_announcements
- announcement_branch
- announcement_receives

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**

	// TO DO

### Emergency announcements
**1. URL**

- GET: {URL}/announcements/emergency

**2. Description**

- Get emergency announcement and update is_read of the emergency announcement is true

**3. Last modify**

	// TO DO

**4. Parameter**

> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "",
        data: {
            id,
            title,
            content,
            start_time_at,
            is_emergency,
            type,
            is_read
        }
    }
- 500:
    {
        status: 500,
        message: "Occur an error while showing announcement."
        errors: []
    }
```

**6. Code handle**

- Controller - function:
    + AnnouncementController@getEmergencyAnnouncement
- Service:
    + App\Services/AnnouncementService
- Repository:
    + App\Repositories\AnnouncementRepository

**7. Table get data**

- enigma_announcements
- announcement_branch
- announcement_receives

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**

	// TO DO

### Show announcements
**1. URL**

- GET: {URL}/announcements/{id}

**2. Description**

- Show announcement and update is_read of the announcement is true

**3. Last modify**

	// TO DO

**4. Parameter**

- id: required

> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "",
        data: {
            id,
            title,
            content,
            start_time_at,
            is_emergency,
            type,
            is_read
        }
    }
- 404:
    {
        status: 404,
        message: "Announcement is not found."
        errors: []
    }
- 500:
    {
        status: 500,
        message: "Occur an error while showing announcement."
        errors: []
    }
```

**6. Code handle**

- Controller - function:
    + AnnouncementController@show
- Service:
    + App\Services/AnnouncementService
- Repository:
    + App\Repositories\AnnouncementRepository

**7. Table get data**

- enigma_announcements
- announcement_branch
- announcement_receives

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**

	// TO DO

### Read announcement
**1. URL**

- PUT: {URL}/announcements/{id}/read

**2. Description**

- Read announcement(update is_read of the announcement is true)

**3. Last modify**

	// TO DO

**4. Parameter**

 - id: required

> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "",
        data: {
            is_read:true
        }
    }
- 404:
    {
        status: 404,
        message: "Announcement is not found."
        errors: []
    }
- 500:
    {
        status: 500,
        message: "Occur an error while reading announcement."
        errors: []
    }
```

**6. Code handle**

- Controller - function:
    + AnnouncementController@read
- Service:
    + App\Services/AnnouncementService
    + App\Services/AnnouncementReceiveService
- Repository:
    + App\Repositories\AnnouncementRepository
    + App\Repositories\AnnouncementReceivesRepository

**7. Table get data**

- enigma_announcements
- announcement_branch
- announcement_receives

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**

	// TO DO

### Read all announcement
**1. URL**

- PUT: {URL}/announcements/read-all

**2. Description**

- Read all announcement(update is_read of the announcement is true)

**3. Last modify**

	// TO DO

**4. Parameter**

> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "",
        data: {
            is_read_all: true
        }
    }
- 500:
    {
        status: 500,
        message: "Occur an error while reading all announcement."
        errors: []
    }
```

**6. Code handle**

- Controller - function:
    + AnnouncementController@readAll
- Service:
    + App\Services/AnnouncementService
    + App\Services/AnnouncementReceiveService
- Repository:
    + App\Repositories\AnnouncementRepository
    + App\Repositories\AnnouncementReceivesRepository

**7. Table get data**

- enigma_announcements
- announcement_branch
- announcement_receives

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**

	// TO DO

## Payment request
### Get payment fee setting
**1. URL**

- GET: {URL}/payment-request/fee-setting

**2. Description**

- Get payment fee setting

**3. Last modify**

	// TO DO

**4. Parameter**

> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "",
        data: {
            consumption_tax_flag,
            upper_limit_per_once,
            lower_limit_per_once,
            prepayment_fee,
            bellow_tfee_same_bank,
            over_tfee_same_bank,
            bellow_tfee_diff_bank,
            over_tfee_diff_bank,
            bank_name,
            bank_code,
            upper_limit_rate,
            upper_limit_total_per_day,
            consumption_tax,
            remain_payment,
            max_remain_today_prepaid,
            max_payment_include_fee
        }
    }
- 403:
    {
        status: 403,
        message:
            "Because it is not within the prepayment acceptance period, so you can not apply prepayment.",
            "Employee hasn't been registered kintai yet."
        errors: []
    }
- 422:
    {
        status: 422,
        message: "Data validation failed"
        errors: []
    }
- 500:
    {
        status: 500,
        message: "Occur an error while showing prepaid setting."
        errors: []
    }
```

**6. Code handle**

- Controller - function:
    + EmployeePaymentRequestController@getPaymentFeeSetting
- Service:
    + App\Services\PaymentRequest\GetPaymentFeeSettingService
    + App\Services\PaymentRequest\EmployeeSalaryService
    + App\Services\PaymentRequest\CompanyScheduleService
    + App\Services\PaymentRequest\CompanySettingService
    + App\Services\PaymentRequest\BranchScheduleService
- Repository:
    + App\Repositories\EmployeePaymentRequestRepository
    + App\Repositories\CompanyPaymentScheduleRepository
    + App\Repositories\EmployeeSalaryRepository
    + App\Repositories\CompanyPrepaidSettingRepository
    + App\Repositories\BranchPrepaidScheduleRepository

**7. Table get data**

- employee_salaries
- company_payment_schedules
- company_prepaid_setting
- enigma_bank_settings
- branch_prepaid_schedules
- branch_prepaid_settings
- employee_payment_requests

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**

	// TO DO

### Get payment request
**1. URL**

- GET: {URL}/payment-request

**2. Description**

- Get list payment requests

**3. Last modify**

	// TO DO

**4. Parameter**

- month:
    + integer
    + between:1,12
- year:
    + digits:4
    + date_format:Y

> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "",
        data: {
            item: [
                {
                    id,
                    total,
                    status,
                    comment,
                    created_at,
                    updated_at,
                    applied_at,
                    status_object: {
                        id,
                        name,
                        message
                    }
                }
            ],
            total_payment
        }
    }

- 422:
    {
        status: 422,
        message: "Data validation failed"
        errors: []
    }

- 500:
    {
        status: 500,
        message: "Occur an error while listing payment request."
        errors: []
    }
```

**6. Code handle**

- Controller - function:
    + EmployeePaymentRequestController@index
- Service:
    + App\Services\PaymentRequest\HistoryPaymentService
    + App\Services\ReportPaymentRequests\DatetimeHandle
- Repository:
    + App\Repositories\EmployeePaymentRequestRepository

**7. Table get data**

- employee_payment_requests

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**

	// TO DO

### Get available payment
**1. URL**

- GET: {URL}/payment-request/available-payment

**2. Description**

- Get available payment

**3. Last modify**

	// TO DO

**4. Parameter**

> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "",
        data: {
            remain_payment,
            id,
            amount_of_salary,
            missing_amount
        }
    }
- 200:
    {
        status: 200,
        message: ""
        data: {
            remain_payment: null,
            id: null,
            amount_of_salary: 0,
            missing_amount: null,
            messages: "Because it is not within the prepayment acceptance period, so you can not apply prepayment."|"Employee hasn't been registered kintai yet."			
        }
    }
- 500:
    {
        status: 500,
        message: "Occur an error while showing available payment info."
        errors: []
    }
```

**6. Code handle**

- Controller - function:
    + EmployeePaymentRequestController@getAvailablePayment
- Service:
    + App\Services\PaymentRequest\GetAvailablePaymentService
    + App\Services\PaymentRequest\EmployeeSalaryService
    + App\Services\PaymentRequest\GetPaymentFeeSettingService
    + App\Services\PaymentRequest\CompanySettingService
    + App\Services\PaymentRequest\CompanyScheduleService
    + App\Services\PaymentRequest\BranchScheduleService
- Repository:
    + App\Repositories\EmployeeSalaryRepository
    + App\Repositories\CompanyPaymentScheduleRepository
    + App\Repositories\EmployeePaymentRequestRepository
    + App\Repositories\CompanyPrepaidSettingRepository
    + App\Repositories\BranchPrepaidScheduleRepository

**7. Table get data**
	
- employee_salaries
- employee_kintais
- company_payment_schedules
- company_prepaid_setting
- enigma_bank_settings
- branch_prepaid_schedules
- branch_prepaid_settings
- employee_payment_requests

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**

	// TO DO	

### List history month
**1. URL**

- GET: {URL}/payment-request/month

**2. Description**

- Get list all months that have payment request

**3. Last modify**

	// TO DO

**4. Parameter**

> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "",
        data: [
            {
                year,
                month,
                total_processing
            }
        ]
    }
- 500:
    {
        status: 500,
        message: "Occur an error while listing month."
        errors: []
    }
```

**6. Code handle**

- Controller - function:
    + EmployeePaymentRequestController@listHistoryMonth
- Service:
    + App\Services\PaymentRequest\HistoryPaymentService
- Repository:
    + App\Repositories\EmployeePaymentRequestRepository

**7. Table get data**

- employee_payment_requests

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**

	// TO DO

### Show payment request
**1. URL**

- GET: {URL}/payment-request/{id}

**2. Description**

- Show detail of payment request

**3. Last modify**

	// TO DO

**4. Parameter**

- id: required

> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "",
        data: {
            id,
            amount_of_salary,
            total_fee_include_tax,
            total,
            status,
            comment,
            applied_at,
            created_at,
            status_object: {
                id,
                name,
                message
            }
        }
    }
- 404:
    {
        status_code: 404,
        message: "Payment request is not found",
        errors: []
    }
- 500:
    {
        status: 500,
        message: "Occur an error while showing payment request."
        errors: []
    }
```

**6. Code handle**

- Controller - function:
    + EmployeePaymentRequestController@show
- Service:
    + App\Services\PaymentRequest\ShowPaymentRequestService
- Repository:
    + App\Repositories\EmployeePaymentRequestRepository

**7. Table get data**

- employee_payment_requests

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**

	// TO DO

### Add payment request
**1. URL**

- POST: {URL}/payment-request

**2. Description**

- Add payment request
- Notice: Send mail when apply error (employee name, apply date,amount salary, error item, error detail)

**3. Last modify**

	// TO DO

**4. Parameter**

- amount_of_salary:
    + required
    + integer
    + regex:"^[0-9]+$"
    + modulus:1000
- captcha_id:
    + required
- captcha:
    + required
    + captcha

> **5. Response**

```php
- 200|201:
    {
        status_code: 200|201,
        message: "Process payment request is complete.",
        data: {
            id,
            total,
            total_fee_include_tax,
            received_money,
            complete_message,
            apply_today,
            status
        }
    }
- 403:
    {
        status: 403,
        message: "Because it is not within the prepayment acceptance period, so you can not apply prepayment."| "Employee hasn't been registered kintai yet."
        errors: []

    }
- 422:
    {
        status: 422,
        message: "Data validation failed"
        errors: []
    }
- 500:
    {
        status: 500,
        message: "Occur an error while adding a payment request."
        errors: []
    }
```

**6. Code handle**

- Controller - function:
    + EmployeePaymentRequestController@store
- Service:
    + App\Services\PaymentRequest\StorePaymentRequestService
    + App\Services\PaymentRequest\EmployeeSalaryService
    + App\Services\PaymentRequest\GetPaymentFeeSettingService
    + App\Services\PaymentRequest\CompanyScheduleService
    + App\Services\PaymentRequest\CompanySettingService
    + App\Services\PaymentRequest\BranchScheduleService
    + App\Services\PaymentRequest\ValidatePrepaymentSalary
    + App\Services\PaymentRequest\BankAPIService
- Repository:
    + App\Repositories\EmployeePaymentRequestRepository
    + App\Repositories\CompanyPaymentScheduleRepository
    + App\Repositories\CompanyPrepaidSettingRepository
    + App\Repositories\BranchPrepaidScheduleRepository
    + App\Repositories\EmployeeSalaryRepository
    + App\Repositories\EmployeeKintaiRepository

**7. Table get data**
	
- employee_salaries
- employee_kintais
- company_payment_schedules
- company_prepaid_setting
- enigma_bank_settings
- branch_prepaid_schedules
- branch_prepaid_settings
- employee_payment_requests

**8. Table change data(insert, update, delete)**
	
	// TO DO

**9. SQL**

	// TO DO

### Validate payment request
**1. URL**

- POST: {URL}/payment-request/validate

**2. Description**

- Validate prepayment request

**3. Last modify**

	// TO DO

**4. Parameter**

- amount_of_salary:
    + required
    + integer
    + regex:"^[0-9]+$"
    + modulus:1000

> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "Validate successfully.",
        data: {
            is_valid,
            is_same_bank,
            amount_of_salary,
            total_fee_include_tax,
            received_money,
        }
    }
- 403:
    {
        status: 403,
        message: "Because it is not within the prepayment acceptance period, so you can not apply prepayment."| "Employee hasn't been registered kintai yet."
        errors: []

    }
- 422:
    {
        status: 422,
        message: "Data validation failed"
        errors: {
            is_valid:false			
        }

    }
- 500:
    {
        status: 500,
        message: "Data validation failed"
        errors: []
    }
```

**6. Code handle**

- Controller - function:
    + EmployeePaymentRequestController@validatePrepaymentSalary
- Service:
    + App\Services\PaymentRequest\ValidatePrepaymentSalary
    + App\Services\PaymentRequest\EmployeeSalaryService
    + App\Services\PaymentRequest\GetPaymentFeeSettingService
    + App\Services\PaymentRequest\CompanyScheduleService
    + App\Services\PaymentRequest\CompanySettingService
    + App\Services\PaymentRequest\BranchScheduleService
- Repository:
    + App\Repositories\EmployeePaymentRequestRepository
    + App\Repositories\CompanyPaymentScheduleRepository
    + App\Repositories\CompanyPrepaidSettingRepository
    + App\Repositories\BranchPrepaidScheduleRepository
    + App\Repositories\EmployeeSalaryRepository
    + App\Repositories\EmployeeKintaiRepository

**7. Table get data**
	
- employee_salaries
- company_payment_schedules
- company_prepaid_setting
- enigma_bank_settings
- branch_prepaid_schedules
- branch_prepaid_settings
- employee_payment_requests

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**

	// TO DO

### List payment request today
**1. URL**

- GET: {URL}/payment-request-today

**2. Description**

- Get list payment request today

**3. Last modify**

	// TO DO

**4. Parameter**

> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "",
        data: {
            item: [
                {
                    id,
                    total,
                    status,
                    comment,
                    created_at,
                    updated_at,
                    applied_at,
                    status_object: {
                        id,
                        name,
                        message
                    }
                }
            ],
            total_payment
        }
    }
- 422:
    {
        status: 422,
        message: "Data validation failed"
        errors: []
    }
- 500:
    {
        status: 500,
        message: "Occur an error while listing payment request."
        errors: []
    }
```

**6. Code handle**

- Controller - function:
    + EmployeePaymentRequestController@listPaymentRequestToday
- Service:
    + App\Services\PaymentRequest\HistoryPaymentService
    + App\Services\ReportPaymentRequests\DatetimeHandle
- Repository:
    + App\Repositories\EmployeePaymentRequestRepository

**7. Table get data**
	
- employee_payment_requests

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

## Csrf token
**1. URL**

- GET: {URL}/csrf-token

**2. Description**

- Get csrf token

**3. Last modify**

	// TO DO

**4. Parameter**

> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "",
        data
    }
```

**6. Code handle**

- Controller - function:
    + CommonApiController@getCsrfToken
- Service:
- Repository:

**7. Table get data**

	// TO DO

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**

	// TO DO

## Captcha
### Generate captcha
**1. URL**

- GET: {URL}/captcha-images

**2. Description**

- Generate captcha

**3. Last modify**

	// TO DO

**4. Parameter**

> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "Generate captcha success.",
        data: {
            captcha_url,
            captcha_id
        }
    }
- 500:
    {
        status: 500,
        message: "Generate captcha fail."
        errors: []
    }
```

**6. Code handle**

- Controller - function:
    + \App\Libs\Captcha\Controller\CaptchaController@generateCaptcha
- Service:
- Repository:

**7. Table get data**

	// TO DO

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**

	// TO DO

### Get captcha
**1. URL**

- GET: {URL}/captcha[/{config}]

**2. Description**

- Get captcha

**3. Last modify**

	// TO DO

**4. Parameter**

- captchaId

> **5. Response**

```php
- \Intervention\Image\ImageManager
```

**6. Code handle**

- Controller - function:
    + \App\Libs\Captcha\Controller\CaptchaController@getCaptcha
- Service:
- Repository:

**7. Table get data**

	// TO DO

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

