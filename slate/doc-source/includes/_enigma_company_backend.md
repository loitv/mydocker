# Enigma Company Backend API

> Artisan command:

```php
cd /path/to/enigma-company_backend

php artisan
```
- [Product requirements](https://neo-universe.atlassian.net/wiki/spaces/ENIGMA/pages/85117753/Services)

- [Bitbucket](https://bitbucket.org/nldanang/enigma-company_backend)

- Basic authentication: `enigma/cie1yiVa`


## Login
### Login manager
**1. URL**

- POST: {URL}/authenticate

**2. Description**

- Login manager

**3. Last modify**

	// TO DO

**4. Parameter**

- company_code:
	+ required
	+ type_code
	+ max:255
- manager_code:
	+ required
	+ type_code
	+ max:255
- manager_password:
    + require_trim
    + type_password
	+ length(6, 255)

> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "Login success",
        data: {
            token,
            user_info: {
                id,
                company_id,
                code,
                email,
                name,
                name_kana,
                role,
                phone_number,
                has_branch_admin,
                is_super_admin,
                company: {
                    id,
                    name,
                    name_kana,
                    code,
                    zip_code,
                    address1,
                    address2,
                    department_name_first_part,
                    department_name_middle_part,
                    department_name_last_part
                }
            },
            enable_two_factor
        }
    }

- 401:
    {
        status_code: 401,
        message: "Your login information was incorrect.",
        errors: []
    }

- 422:
    {
        status_code: 422    ,
        message: "Your login information was incorrect.",
        errors: []
    }

- 500:
    {
        
        status_code: 500,
        message: "Your login information was incorrect.",
        errors: []
    }
```

**6. Code handle**

- Controller - function:
	+ AuthenticateController@login
- Service:
- Repository:
    + App\Repositories\Eloquent\ManagerRepository
    
**7. Table get data**

- managers
- companies

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

### Login enigma user
**1. URL**

- POST: {URL}/authenticate/admin

**2. Description**

- Login enigma user

**3. Last modify**

	// TO DO

**4. Parameter**

- company_code:
	+ required
	+ type_code
	+ max:255
- email:
	+ required
	+ email
	+ max:255
	+ exists:enigma_users,email,deleted_at,NULL,role, EnigmaUser::ROLE_ADMIN
- password:
    + required
    + type_password
	+ length(6, 255)

> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "Login success",
        data: {
            enable_two_factor
            user_id
            secret_token
        }
    }

- 401:
    {
        status_code: 401,
        message: "Your login information was incorrect.",
        errors: []
    }

- 422:
    {
        status_code: 422    ,
        message: "Your login information was incorrect.",
        errors: []
    }

- 500:
    {
        
        status_code: 500,
        message: "Your login information was incorrect.",
        errors: []
    }
```

**6. Code handle**

- Controller - function:
	+ AuthenticateController@loginEnigmaUser
- Service:
- Repository:
    + App\Repositories\Eloquent\EnigmaUserRepository
    + App\Repositories\Eloquent\CompanyRepository

**7. Table get data**

- enigma_users

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

### Authenticate with 2 factor
**1. URL**

- POST: {URL}/authenticate/google/callback

**2. Description**

- Handle google callback

**3. Last modify**

	// TO DO

**4. Parameter**

- redirectUri: nullable
- user_id:
- secret_token:

> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "Login success",
        data: {
            token,
            user_info: {
                id,
                company_id,
                code,
                email,
                name,
                name_kana,
                role,
                phone_number,
                has_branch_admin,
                is_super_admin,
                company: {
                    id,
                    name,
                    name_kana,
                    code,
                    zip_code,
                    address1,
                    address2,
                    department_name_first_part,
                    department_name_middle_part,
                    department_name_last_part
                }
            },
            enable_two_factor
        }
    }

- 401:
    {
        status_code: 401,
        message: "Your login information was incorrect.|The login time has expired, please login again.",
        errors: []
    }

- 422:
    {
        status_code: 422,
        message: "Invalid input data.",
        errors: []
    }
    
- 403:
    {
        status_code: 403,
        message: "You do not have access to this page.",
        errors: []
    }

- 500:
    {
        
        status_code: 500,
        message: "Your login information was incorrect.",
        errors: []
    }
```

**6. Code handle**

- Controller - function:
	+ AuthenticateController@handleGoogleCallback
- Service:
    + App\Services\GoogleOauthService
- Repository:
    + App\Repositories\Eloquent\ManagerRepository

**7. Table get data**

- managers

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

### Validate google2fa token
**1. URL**

- POST: {URL}/2fa/validate

**2. Description**

- Post validate token.

**3. Last modify**

	// TO DO

**4. Parameter**

- user_id:
    + required
- secret_token:
    + required
- google2fa_number:
    + required

> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "Login success",
        data: {
            token,
            user_info: {
                id,
                company_id,
                code,
                email,
                name,
                name_kana,
                role,
                phone_number,
                has_branch_admin,
                is_super_admin,
                company: {
                    id,
                    name,
                    name_kana,
                    code,
                    zip_code,
                    address1,
                    address2,
                    department_name_first_part,
                    department_name_middle_part,
                    department_name_last_part
                }
            },
            enable_two_factor
        }
    }

- 401:
    {
        status_code: 401,
        message: "Your login information was incorrect.|The login time has expired, please login again.",
        errors: []
    }

- 422:
    {
        status_code: 422,
        message: "Code number invalid.",
        errors: []
    }

- 500:
    {
        
        status_code: 500,
        message: "Your login information was incorrect.",
        errors: []
    }
```

**6. Code handle**

- Controller - function:
	+ AuthenticateController@postValidateToken
- Service:
    + App\Services\Google2FAService
- Repository:
    + App\Repositories\Eloquent\ManagerRepository

**7. Table get data**

- managers

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

## Password
### Email reset password
**1. URL**

- POST: {URL}/password/email

**2. Description**

- Get link reset password email.
- Notice: This function will send link reset password email for manager who reset password (token, url, name).

**3. Last modify**

	// TO DO

**4. Parameter**

- email:
	+ required
	+ email
	+ exists:managers,email
- company_code:
	+ required
	+ type_code
	+ max:20
	+ exists_lower:companies,code

> **5. Response**

```php
- 200:
    {
        status: 200,
        message: "Send password reset success"
        data: []
    }


- 422:
    {
        status: 422,
        message: "Send password reset failed"
        errors: {
            "company_code": [
                "The Company code field is required.",
                "The selected Company code is invalid.",
                "The Company code may not be greater than 20 characters.",
                "The Company code should not contain any special characters.",
            ],
            "email": [
                "The Email field is required.",
                "The Email must be a valid email address.",
                "The selected Email is invalid."
            ]
        }
    }

- 500:
    {
        status: 500,
        message: "Send password reset failed"
        errors: []
    }
```

**6. Code handle**

- Controller - function:
	+ PasswordController@getLinkResetPasswordEmail
- Repositories:
- Service:
	+ App\Services\Password\GetLinkResetPasswordService
	+ App\Services\Password\CommonService

**7. Table get data**

- manager_password_resets
- managers

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**

	// TO DO

### Reset password
**1. URL**

- POST: {URL}/password/reset

**2. Description**

- Reset manager password by email.
- Notice: This function will send changed password mail (manager name and url login).

**3. Last modify**

	// TO DO

**4. Parameter**

- token:
	+ required
	+ exists:manager_password_resets,token
- email:
	+ required
	+ email
	+ exists:manager_password_resets,email
- password:
	+ required
	+ require_trim
	+ type_password
	+ length(6, 255)
- password_confirmation:
	+ require_same:password

> **5. Response**

```php
- 200:
	{
		status: 200,
		message: "Reset password success"
		data: []
	}

- 422:
	{
		status: 422,
		message: "Reset password failed"
		errors: []
    }

- 500:
	{
		status: 500,
		message: "Reset password failed"
		errors: []
	}
```

**6. Code handle**

- Controller - function:
	+ PasswordController@resetByEmail
- Service:
	+ App\Services\Password\ResetPasswordService
	+ App\Services\Password\CommonService

**7. Table get data**

- manager_password_resets
- managers

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**

	// TO DO

### Check url reset password
**1. URL**

- GET: {URL}/password/check-url

**2. Description**

- Check url reset password.

**3. Last modify**

	// TO DO

**4. Parameter**

- token:
	+ required
	+ integer

> **5. Response**

```php
- 200:
	{
		status: 200,
		message: ""
		data: {
			is_valid: true 
		}
	}
	
- 200:
    {
        status: 200,
        message: ""
        data: {
            is_valid: false,
            message: "This URL is invalid."   
        }
    }

- 500:
	{
		status: 500,
		message: "Occur an error while checking password reset url."
		errors: []
	}
```

**6. Code handle**

- Controller - function:
	+ PasswordController@checkResetUrl
- Repositories:
    + App\Repositories\Eloquent\ManagerPasswordResetRepository
- Service:
	+ App\Services\Password\CheckResetPasswordUrlService
	+ App\Services\Password\CommonService

**7. Table get data**

- manager_password_resets

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**

	// TO DO

## Version api
**1. URL**

- GET: {URL}/version

**2. Description**

- API get version.

**3. Last modify**

	// TO DO

**4. Parameter**

> **5. Response**

```php	
{
    message: "Enigma version release"
    data: {
        version: "v2.2.1"
    }
}
```

**6. Code handle**

	// TO DO

**7. Table get data**

	// TO DO

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

## Logout
**1. URL**

- GET: {URL}/logout

**2. Description**

- Manager logout.

**3. Last modify**

	// TO DO

**4. Parameter**

> **5. Response**

```php	
- 200:
    {
        status: 200,
        message: "logout success"
        data: []
    }
```    

**6. Code handle**

- Controller - function:
    + AuthenticateController@logout

**7. Table get data**

	// TO DO

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**

	// TO DO

## Common
### Get status of employee
**1. URL**

- GET: {URL}/employees/status

**2. Description**

- Get list status of employee

**3. Last modify**

	// TO DO

**4. Parameter**

> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "",
        data: [
            {
                id
                name
            }
        ]
    }
```

**6. Code handle**

- Controller - function:
	+ EmployeeController@getListStatus
- Service:
- Repository:
    
**7. Table get data**

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

### List contract types
**1. URL**

- GET: {URL}/employees/contract-types

**2. Description**

- Get contract type

**3. Last modify**

	// TO DO

**4. Parameter**

> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "",
        data: [
            {
                id
                name
            }
        ]
    }
```

**6. Code handle**

- Controller - function:
	+ EmployeeController@getContractType
- Service:
- Repository:
    + App\Repositories\Eloquent\EmployeeRepository
    
**7. Table get data**

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

### List deposit types
**1. URL**

- GET: {URL}/employees/deposit-types

**2. Description**

- Get deposit type

**3. Last modify**

	// TO DO

**4. Parameter**

> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "",
        data: [
            {
                id
                name
            }
        ]
    }
```

**6. Code handle**

- Controller - function:
	+ EmployeeController@getDepositType
- Service:
- Repository:
    + App\Repositories\Eloquent\EmployeeRepository
    
**7. Table get data**

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

### List stop reasons
**1. URL**

- GET: {URL}/employees/stop-reasons

**2. Description**

- Get common stopped reasons

**3. Last modify**

	// TO DO

**4. Parameter**

> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message "",
        data: [
            {
                id
                name
            }
        ]
    }
    
- 500:
    {
        status_code: 500,
        message "Stop Reason cannot be got",
        errors: []
    }
```

**6. Code handle**

- Controller - function:
	+ EmployeeController@getStopReasons
- Service:
- Repository:
    + App\Repositories\Eloquent\EmployeeStatusStopRepository
    
**7. Table get data**

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

### List status import
**1. URL**

- GET: {URL}/imports/status

**2. Description**

- List imported file status

**3. Last modify**

	// TO DO

**4. Parameter**

> **5. Response**

```php
- 200:
    {
        "status_code": 200,
        "message": "",
        "data": [
            {
                id
                name
            }
        ]
    }
```

**6. Code handle**

- Controller - function:
	+ ImportEmployeeController@getStatus
- Service:
- Repository:
    
**7. Table get data**

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

### List company kintai
**1. URL**

- GET: {URL}/company-kintais/patterns

**2. Description**

- Get company kintai patterns

**3. Last modify**

	// TO DO

**4. Parameter**

> **5. Response**

```php
- 200:
    {
        "status_code": 200,
        "message": "",
        "data": [
            {
                id,
                name,
                data: [
                    {
                        id,
                        name,
                        type
                    }
                ]
            }
        ]
    }
    
- 500:
    {
        status_code: 500,
        message: "Company kintai patterns can not be displayed.",
        errors: []
    }
```

**6. Code handle**

- Controller - function:
	+ CompanyKintaiController@getPatterns
- Service:
- Repository:
    + App\Repositories\Eloquent\CompanyKintaiRepository
    
**7. Table get data**

- company_kintais

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

### Check permission
**1. URL**

- GET: {URL}/common/permission/{type}/{action}/{id}

**2. Description**

- Check permission for show/update/delete manager by admin

**3. Last modify**

	// TO DO

**4. Parameter**

> **5. Response**

```php
- 200:
    {
        "status_code": 200,
        "message": "",
        "data": [
            {
                $action: true|false
            }
        ]
    }
    
- 500:
    {
        status_code: 500,
        message: "Failed.",
        errors: []
    }
```

**6. Code handle**

- Controller - function:
	+ CommonApiController@checkPermission
- Service:
- Repository:
    + App\Repositories\Eloquent\ManagerRepository
    + App\Repositories\Eloquent\BranchRepository
    + App\Repositories\Eloquent\BranchPrepaidSettingRepository
    
**7. Table get data**

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

### Get roles of manager
**1. URL**

- GET: {URL}/managers/roles

**2. Description**

- Get roles of manager

**3. Last modify**

	// TO DO

**4. Parameter**

> **5. Response**

```php
- 200:
    {
        "status_code": 200,
        "message": "",
        "data": [
            {
                id,
                name
            }
        ]
    }
```

**6. Code handle**

- Controller - function:
	+ ManagerController@getManagerRoles
- Service:
- Repository:
    + App\Repositories\Eloquent\ManagerRepository
    
**7. Table get data**

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

### Get type of payment cycle
**1. URL**

- GET: {URL}/companies/cycle-type

**2. Description**

- Get types of company cycle

**3. Last modify**

	// TO DO

**4. Parameter**

> **5. Response**

```php
- 200:
    {
        "status_code": 200,
        "message": "",
        "data": [
            {
                id,
                name
            }
        ]
    }
```

**6. Code handle**

- Controller - function:
	+ CompanyController@getCompanyCycleTypes
- Service:
- Repository:
    + App\Repositories\Eloquent\CompanyPaymentCycleRepository
    
**7. Table get data**

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

### Get timing for payment cycle
**1. URL**

- GET: {URL}/companies/timing/{cycleType}

**2. Description**

- Get timing for payment cycle

**3. Last modify**

	// TO DO

**4. Parameter**

> **5. Response**

```php
- 200:
    {
        "status_code": 200,
        "message": "",
        "data": [
            {
                id,
                name
            }
        ]
    }
```

**6. Code handle**

- Controller - function:
	+ CompanyController@getTiming
- Service:
- Repository:
    + App\Repositories\Eloquent\CompanyPaymentCycleRepository
    
**7. Table get data**

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

### List time periods of employee kintai
**1. URL**

- GET: {URL}/employee-kintais/list-of-time-periods

**2. Description**

- Get payment request by period

**3. Last modify**

	// TO DO

**4. Parameter**

> **5. Response**

```php
- 200:
    {
        "status_code": 200,
        "message": "",
        "data": [
            {
                id,
                name
            }
        ]
    }
```

**6. Code handle**

- Controller - function:
	+ CommonApiController@getEmployeeKintaiPeriod
- Service:
- Repository:
    
**7. Table get data**

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

### List type payment request
**1. URL**

- GET: {URL}/employee-payment-requests/types

**2. Description**

- Get payment request type

**3. Last modify**

	// TO DO

**4. Parameter**

> **5. Response**

```php
- 200:
    {
        "status_code": 200,
        "message": "",
        "data": [
            {
                id,
                name
            }
        ]
    }
```

**6. Code handle**

- Controller - function:
	+ CommonApiController@getPaymentRequestType
- Service:
- Repository:
    
**7. Table get data**

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

### List period of employee payment request
**1. URL**

- GET: {URL}/employees/{id}/schedule-selection

**2. Description**

- Get schedule selection

**3. Last modify**

	// TO DO

**4. Parameter**

> **5. Response**

```php
- 200:
    {
        "status_code": 200,
        "message": "",
        "data": [
            {
                id,
                name
            }
        ]
    }
```

**6. Code handle**

- Controller - function:
	+ CommonApiController@getScheduleSelection
- Service:
    + App\Services\CommonApis\ScheduleSelectionService
- Repository:
    + App\Repositories\Eloquent\CompanyPaymentScheduleRepository
    
**7. Table get data**

- company_payment_schedules

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

### Get csrf token
**1. URL**

- GET: {URL}/csrf-token

**2. Description**

- Get csrf token

**3. Last modify**

	// TO DO

**4. Parameter**

> **5. Response**

```php
- 200:
    {
        "status_code": 200,
        "message": "",
        "data": ""
    }
```

**6. Code handle**

- Controller - function:
	+ CommonApiController@getCsrfToken
- Service:
- Repository:
    
**7. Table get data**

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

### List schedule selection
**1. URL**

- GET: {URL}/schedule-selection

**2. Description**

- Get schedule selection

**3. Last modify**

	// TO DO

**4. Parameter**

> **5. Response**

```php
- 200:
    {
        "status_code": 200,
        "message": "",
        "data": [
            {
                id,
                name
            }
        ]
    }
```

**6. Code handle**

- Controller - function:
	+ CommonApiController@getScheduleSelection
- Service:
    + App\Services\CommonApis\ScheduleSelectionService
- Repository:
    + App\Repositories\Eloquent\CompanyPaymentScheduleRepository
    
**7. Table get data**

- company_payment_schedules

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

### List status of employee payment request
**1. URL**

- GET: {URL}/employee-payment-requests/status

**2. Description**

- Get list status of employee payment request

**3. Last modify**

	// TO DO

**4. Parameter**

> **5. Response**

```php
- 200:
    {
        "status_code": 200,
        "message": "",
        "data": [
            {
                id,
                name
            }
        ]
    }
```

**6. Code handle**

- Controller - function:
	+ CommonApiController@getStatusPaymentRequest
- Service:
- Repository:
    
**7. Table get data**

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

### Get type of prepaid usage list type
**1. URL**

- GET: {URL}/prepaid-usage-types

**2. Description**

- Get type of prepaid usage list type

**3. Last modify**

	// TO DO

**4. Parameter**

> **5. Response**

```php
- 200:
    {
        "status_code": 200,
        "message": "",
        "data": [
            {
                id,
                name
            }
        ]
    }
```

**6. Code handle**

- Controller - function:
	+ CommonApiController@getPrepaidUsagelistTypes
- Service:
- Repository:
    
**7. Table get data**

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

## Download- Export deductible employee csv
**1. URL**

- GET: {URL}/branch-payment-requests/export-csv

**2. Description**

- Export deductible employee CSV

**3. Last modify**

	// TO DO

**4. Parameter**

- schedule_id
- branch_id
	
> **5. Response**

```php
Returns the number of characters read from the handle and passed through to the output (file csv with name have format EmployeeDeductionsCSV_%s.csv)
```

**6. Code handle**

- Controller - function:
	+ PaymentRequestController@exportDeductibleCsv
- Service:
    + App\Services\PaymentRequestService
- Repository:
    + App\Repositories\Eloquent\EmployeeDeductionRepository
    + App\Repositories\Eloquent\EmployeePaymentRequestRepository
    + App\Repositories\Eloquent\BranchRepository
    
**7. Table get data**

- employee_deductions
- employee_payment_requests
- branches

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

## Employee
### Export employee csv
**1. URL**

- GET: {URL}/employees/export-csv

**2. Description**

- Export deductible employee CSV

**3. Last modify**

	// TO DO

**4. Parameter**

- name_kana: nullable
- code: nullable
- status: nullable
- name: nullable
- branch_id: nullable
- updated_from: nullable
- updated_to: nullable
- approved_from:
- approved_to: nullable
- start_from: nullable
- start_to: nullable
- end_from: nullable
- end_to: nullable
- sort_column: nullable
- sort_direction: nullable

> **5. Response**

```php
- 200:
Returns the number of characters read from the handle and passed through to the output (file csv with name have format employee-date('Ymd').csv)

- 500:
    {
        
        status_code: 500,
        message: "Employee cannot be listed.",
        errors: []
    }
```

**6. Code handle**

- Controller - function:
	+ EmployeeController@exportCsv
- Service:
- Repository:
    + App\Repositories\Eloquent\EmployeeRepository
    
**7. Table get data**

- employees

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

### Delete employees
**1. URL**

- DELETE: {URL}/employees/deletes

**2. Description**

- Delete employees by IDs

**3. Last modify**

	// TO DO

**4. Parameter**

- ids:
- current_password:

> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "Employee has been deleted successfully.",
        data: []
    }

- 403:
    {
        status_code: 403,
        message: "Permission denied.",
        errors: []
    }
   
- 422:
    {
        status_code: 422,
        message: "Employees who are using or temporarily stop the service can not be deleted.",
        errors: []
    }
    
- 500:
    {
        status_code: 500,
        message: "Employee delete fail.",
        errors: []
    }
```

**6. Code handle**

- Controller - function:
	+ EmployeeController@deletes
- Service:
    + App\Services\Employees\DeleteEmployeeService
- Repository:
    + App\Repositories\Eloquent\EmployeeRepository
    + App\Repositories\Eloquent\ManagerRepository
    
**7. Table get data**

- employees
- manager

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

### Update to approved status
**1. URL**

- PUT: {URL}/employees/approve

**2. Description**

- Approve employee for using service

**3. Last modify**

	// TO DO

**4. Parameter**

- ids

> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "Employee has been approved using successfully.",
        data: []
    }
    
- 500:
    {
        status_code: 500,
        message: "Approve employee fail.",
        errors: []
    }
```

**6. Code handle**

- Controller - function:
	+ EmployeeController@updateApprovedStatus
- Service:
- Repository:
    + App\Repositories\Eloquent\EmployeeRepository
    
**7. Table get data**

- employees

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

### Update to stop status
**1. URL**

- PUT: {URL}/employees/stop/{id}

**2. Description**

- Stop employee using service

**3. Last modify**

	// TO DO

**4. Parameter**

- reason:
- reason_detail:
- stopped_at:

> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "Employee has been stopped using successfully.",
        data: []
    }
    
- 422:
    {
        status_code: 422,
        message: "Data validation failed.",
        errors: []
    }
        
- 500:
    {
        status_code: 500,
        message: "Stop employee fail.",
        errors: []
    }
```

**6. Code handle**

- Controller - function:
	+ EmployeeController@updateStoppedStatus
- Service:
    + App\Services\Employees\StopEmployeeService
- Repository:
    + App\Repositories\Eloquent\EmployeeRepository
    + App\Repositories\Eloquent\EmployeeStatusStopRepository
    
**7. Table get data**

- employees
- employee_status_stopped

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

### Update to unstop
**1. URL**

- PUT: {URL}/employees/unstop/{id}

**2. Description**

- Unstop employee in order to continue using service

**3. Last modify**

	// TO DO

**4. Parameter**

> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "Employee has been unstop successfully.",
        data: []
    }
        
- 500:
    {
        status_code: 500,
        message: "Unstop employee fail.",
        errors: []
    }
```

**6. Code handle**

- Controller - function:
	+ EmployeeController@updateUnstoppedStatus
- Service:
    + App\Services\Employees\UnstopEmployeeService
- Repository:
    + App\Repositories\Eloquent\EmployeeRepository
    + App\Repositories\Eloquent\EmployeeStatusStopRepository
    
**7. Table get data**

- employees
- employee_status_stopped

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

### List employees
**1. URL**

- GET: {URL}/employees

**2. Description**

- List employees

**3. Last modify**

	// TO DO

**4. Parameter**

- name_kana: nullable
- code: nullable
- status: nullable
- name: nullable
- branch_id: nullable
- updated_from: nullable
- updated_to: nullable
- approved_from: nullable
- approved_to: nullable
- start_from: nullable
- start_to: nullable
- end_from: nullable
- end_to: nullable
- sort_column: nullable
- sort_direction: nullable
- per_page: nullable

> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "",
        data: {
            total,
            per_page,
            current_page,
            last_page,
            next_page_url,
            prev_page_url,
            from,
            to,
            items: [
                {
                    id,
                    code,
                    email,
                    name,
                    name_kana,
                    phone_number,
                    status,
                    start_contract_time,
                    end_contract_time,
                    company_id,
                    branch_id,
                    created_at,
                    updated_at,
                    is_admin,
                    status_object: {
                        id,
                        name
                    },
                    branch: {
                        id,
                        code,
                        name,
                        name_kana
                    }
                }
            ]
    }
        
- 500:
    {
        status_code: 500,
        message: "Employee cannot be listed.",
        errors: []
    }
```

**6. Code handle**

- Controller - function:
	+ EmployeeController@index
- Service:
    + App\Services\Employees\ListEmployeeService
    + App\Services\Employees\CommonService
- Repository:
    + App\Repositories\Eloquent\EmployeeRepository
    
**7. Table get data**

- employees
- branches

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

### Create employees
**1. URL**

- POST: {URL}/employees

**2. Description**

- Register a employee

**3. Last modify**

	// TO DO

**4. Parameter**

- branch_id:
    + required
    + exists:branches,id
- code:
    + required
    + type_code
    + max:255
    + unique:employees,code
- name:
    + required
    + max:40
- name_kana:
    + is_kana
    + fullsize
    +  max:40
- is_social_insurance:
    + required
    + boolean
- is_employment_insurance:
    + required
    + boolean
- insurance_fee:
    + required_if:insurance_policy,Employee::INSURANCE_PERSONAL
    + integer
    + min:0
    + max:99999999
- insurance_policy:
    + required_if:is_social_insurance,true,1
    + in:Employee::INSURANCE_COMPANY, Employee::INSURANCE_PERSONAL
- email:
    + nullable
    + email
    + max:255
- phone_number:
    + phone_number
    + max:20
- bank_code:
    + required
    + numeric
    + digits_between:4,4
- bank_name_kana:
    + max:255
- bank_branch_code:
    + required
    + numeric
    + digits_between:3,3
- bank_branch_name_kana:
    + max:255
- bank_account_number:
    + required
    + is_halfwidth_number
    + numeric
    + digits_between:7,7
- bank_account_holder_kana:
    + required
    + is_halfwidth_kana_extend
    + max:40
- deposit_type:
    + required
    + in:Employee::DEPOSIT_TYPE_NORMAL, Employee::DEPOSIT_TYPE_CURRENT, Employee::DEPOSIT_TYPE_SAVING
- start_contract_time:
    + ate_format:Y-m-d,Y/m/d
    + nullable
- end_contract_time:
    + date_format:Y-m-d,Y/m/d
    + after:start_contract_time
    + nullable
- contract_type:
    + nullable
    + in:Employee::CONTRACT_TYPE_REGULAR, Employee::CONTRACT_TYPE_PART_TIME, Employee::CONTRACT_TYPE_TEMP_CONTRACT, Employee::CONTRACT_TYPE_DAILY_DISPATCH, Employee::CONTRACT_TYPE_OUTSOURCING_BUSINESS
            
> **5. Response**

```php
- 201:
    {
        status_code: 201,
        message: "Employee has been added successfully.",
        data: {
            id
        }
    }
        
- 422:
    {
        status_code: 422,
        message: "Data validation failed.",
        errors: []
    }
            
- 500:
    {
        status_code: 500,
        message: "Create employee error.",
        errors: []
    }
```

**6. Code handle**

- Controller - function:
	+ EmployeeController@store
- Service:
    + App\Services\EmployeeService
- Repository:
    + App\Repositories\Eloquent\EmployeeRepository
    
**7. Table get data**

- employees

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

### Update employees
**1. URL**

- PUT: {URL}/employees/{id}

**2. Description**

- Update information of employee

**3. Last modify**

	// TO DO

**4. Parameter**

- branch_id:
    + required
    + exists:branches,id
- code:
    + required
    + type_code
    + max:255
    + unique:employees,code
- name:
    + required
    + max:40
- name_kana:
    + is_kana
    + fullsize
    +  max:40
- is_social_insurance:
    + required
    + boolean
- is_employment_insurance:
    + required
    + boolean
- insurance_fee:
    + required_if:insurance_policy,Employee::INSURANCE_PERSONAL
    + integer
    + min:0
    + max:99999999
- insurance_policy:
    + required_if:is_social_insurance,true,1
    + in:Employee::INSURANCE_COMPANY, Employee::INSURANCE_PERSONAL
- email:
    + nullable
    + email
    + max:255
- phone_number:
    + phone_number
    + max:20
- password:
    + min:6
    + max:255
    + nullable
    + require_trim
    + type_password
- password_confirmation:
    + require_same:password
- bank_code:
    + required
    + numeric
    + digits_between:4,4
- bank_name_kana:
    + max:255
- bank_branch_code:
    + required
    + numeric
    + digits_between:3,3
- bank_branch_name_kana:
    + max:255
- bank_account_number:
    + required
    + is_halfwidth_number
    + numeric
    + digits_between:7,7
- bank_account_holder_kana:
    + required
    + is_halfwidth_kana_extend
    + max:40
- deposit_type:
    + required
    + in:Employee::DEPOSIT_TYPE_NORMAL, Employee::DEPOSIT_TYPE_CURRENT, Employee::DEPOSIT_TYPE_SAVING
- start_contract_time:
    + ate_format:Y-m-d,Y/m/d
    + nullable
- end_contract_time:
    + date_format:Y-m-d,Y/m/d
    + after:start_contract_time
    + nullable
- contract_type:
    + nullable
    + in:Employee::CONTRACT_TYPE_REGULAR, Employee::CONTRACT_TYPE_PART_TIME, Employee::CONTRACT_TYPE_TEMP_CONTRACT, Employee::CONTRACT_TYPE_DAILY_DISPATCH, Employee::CONTRACT_TYPE_OUTSOURCING_BUSINESS
                             
> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "Employee has been updated successfully.",
        data: []
    }
        
- 422:
    {
        status_code: 422,
        message: "Data validation failed.",
        errors: []
    }
            
- 500:
    {
        status_code: 500,
        message: "Update employee error.",
        errors: []
    }
```

**6. Code handle**

- Controller - function:
	+ EmployeeController@update
- Service:
    + App\Services\EmployeeService
- Repository:
    + App\Repositories\Eloquent\EmployeeRepository
    
**7. Table get data**

- employees

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

### Show employees
**1. URL**

- GET: {URL}/employees/{id}

**2. Description**

- Show detail of employee

**3. Last modify**

	// TO DO

**4. Parameter**

- id
            
> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "",
        data: {
            id,
            code,
            email,
            name,
            name_kana,
            is_social_insurance,
            is_social_insurance_flag,
            insurance_fee_schedule,
            insurance_policy_schedule,
            is_employment_insurance,
            insurance_fee,
            insurance_policy: {
                id,
                name
            },
            phone_number,
            branch_id,
            company_id,
            status,
            contract_type: {
                id,
                name
            },
            deposit_type: {
                id,
                name
            },
            start_contract_time,
            end_contract_time,
            bank_code,
            bank_name_kana,
            bank_branch_code,
            bank_branch_name_kana,
            bank_account_number,
            bank_account_holder_kana,
            status_object: {
                id,
                name
            },
            company_kintais: [
                {
                    id,
                    name,
                    data: [
                        {
                            id,
                            name,
                            type
                        }
                    ]
                },
                {
                    id,
                    name,
                    data: [
                        {
                            id,
                            name,
                            type
                        }
                    ]
                }
            ],
            stopped_status: {
                reason,
                reason_detail,
                stopped_at
            },
            stopped_reason: {
                id,
                name
            },
            can_show_payment_request,
            branch: {
                id,
                code,
                name,
                name_kana
            },
            previous_branch_schedule
        }
    }

- 403:
    {
        status_code: 403,
        message: "Permission denied.",
        errors: []
    }
        
- 404:
    {
        status_code: 404,
        message: "Employee not found.",
        errors: []
    }
            
- 500:
    {
        status_code: 500,
        message: "Employee cannot be showed.",
        errors: []
    }
```

**6. Code handle**

- Controller - function:
	+ EmployeeController@store
- Service:
    + App\Services\EmployeeService
- Repository:
    + App\Repositories\Eloquent\EmployeeRepository
    + App\Repositories\Eloquent\EmployeeKintaiRepository
    + App\Repositories\Eloquent\CompanyKintaiRepository
    + App\Repositories\Eloquent\EmployeeStatusStopRepository
    
**7. Table get data**

- employees
- employee_kintais
- company_kintais
- employee_status_stopped

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

### Show company kintai patern of employee
**1. URL**

- GET: {URL}/employees/{employeeId}/company-kintais/{companyKintaiId}/pattern

**2. Description**

- List employees kintai by pattern

**3. Last modify**

	// TO DO

**4. Parameter**

- sort_column: nullable
- sort_direction: nullable
- per_page: nullable
            
> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "",
        data: {
            total,
            per_page,
            current_page,
            last_page,
            next_page_url,
            prev_page_url,
            from,
            to,
            items: [
                {
                    id,
                    branch_name,
                    employee_code,
                    employee_name,
                    employee_name_kana,
                    updated_date,
                    total_amount_of_salary,
                    created_at,
                    employee_kintai_values: [
                        {
                            id,
                            value
                        }
                    ]
                }
            ],
            headers: [
                {
                    id,
                    name
                }
            ]
    }
            
- 500:
    {
        status_code: 500,
        message: "cannot be listed.",
        errors: []
    }
```

**6. Code handle**

- Controller - function:
	+ EmployeeKintaiController@listEmployeeByPattern
- Service:
    + App\Services\EmployeeService
- Repository:
    + App\Repositories\Eloquent\EmployeeRepository
    + App\Repositories\Eloquent\EmployeeKintaiRepository
    + App\Repositories\Eloquent\CompanyKintaiRepository
    
**7. Table get data**

- employees
- employee_kintais
- company_kintais

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

### Show employee payment request
**1. URL**

- GET: {URL}/employees/{id}/payment-requests

**2. Description**

- Show payment request

**3. Last modify**

	// TO DO

**4. Parameter**

- schedule_id
            
> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "Employee has been updated successfully.",
        data: {
            total,
            per_page,
            current_page,
            last_page,
            next_page_url,
            prev_page_url,
            from,
            to,
            items: [
                {
                    id,
                    branch_name,
                    employee_code,
                    employee_name,
                    employee_name_kana,
                    updated_date,
                    total_amount_of_salary,
                    created_at,
                    employee_kintai_values: [
                        {
                            id,
                            value
                        }
                    ]
                }
            ],
            headers: [
                {
                    id,
                    name
                }
            ]
    }

- 422:
    {
        status_code: 422,
        message: "Data validation failed.",
        errors: []
    }
               
- 500:
    {
        status_code: 500,
        message: "Employee payment request cannot be showed.",
        errors: []
    }
```

**6. Code handle**

- Controller - function:
	+ PaymentRequestController@show
- Service:
    + App\Services\PaymentRequestService
- Repository:
    + App\Repositories\Eloquent\EmployeeRepository
    + App\Repositories\Eloquent\EmployeePaymentRequestRepository
    
**7. Table get data**

- employees
- employee_payment_requests

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

## Manager
### List managers of company
**1. URL**

- GET: {URL}/companies/managers

**2. Description**

- List managers of company

**3. Last modify**

	// TO DO

**4. Parameter**
            
> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "",
        data: {
            total,
            per_page,
            current_page,
            last_page,
            next_page_url,
            prev_page_url,
            from,
            to,
            items: [
                {
                    id,
                    code,
                    name,
                    name_kana,
                    email,
                    created_at,
                    is_you,
                    branches: [
                        {
                            name,
                            role,
                            id
                        }
                    ],
                    can_delete,
                    can_edit
                },
            ]
    }
               
- 500:
    {
        status_code: 500,
        message: "Manager cannot be listed.",
        errors: []
    }
```

**6. Code handle**

- Controller - function:
	+ ManagerController@listManagersCompany
- Service:
- Repository:
    + App\Repositories\Eloquent\ManagerRepository
    
**7. Table get data**

- managers

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

### List branches of manager
**1. URL**

- GET: {URL}/managers/branches

**2. Description**

- List branches of manager

**3. Last modify**

	// TO DO

**4. Parameter**
            
> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "",
        data: {
            id,
            name
    }
               
- 500:
    {
        status_code: 500,
        message: "Branch cannot be listed.",
        errors: []
    }
```

**6. Code handle**

- Controller - function:
	+ BranchController@listBranches
- Service:
- Repository:
    + App\Repositories\Eloquent\BranchRepository
    
**7. Table get data**

- branches

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

### List managers
**1. URL**

- GET: {URL}/managers

**2. Description**

- List managers by admin

**3. Last modify**

	// TO DO

**4. Parameter**
      
- name: nullable
- name_kana: nullable
- role: nullable
- branch_id: nullable
- created_from: nullable
- created_to: nullable
- per_page: nullable
- sort_column: nullable
- sort_direction: nullable
                
> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "",
        data: {
            total,
            per_page,
            current_page,
            last_page,
            next_page_url,
            prev_page_url,
            from,
            to,
            items: [
                {
                    id,
                    code,
                    name,
                    name_kana,
                    email,
                    created_at,
                    is_you,
                    branches: [
                        {
                            name,
                            role,
                            id
                        }
                    ],
                    can_delete,
                    can_edit
                },
            ]
    }
               
- 500:
    {
        status_code: 500,
        message: "Manager cannot be listed.",
        errors: []
    }
```

**6. Code handle**

- Controller - function:
	+ ManagerController@listManagersCompany
- Service:
- Repository:
    + App\Repositories\Eloquent\ManagerRepository
    
**7. Table get data**

- managers

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

### Show managers
**1. URL**

- GET: {URL}/managers/{id}

**2. Description**

- Show detail manager by admin

**3. Last modify**

	// TO DO

**4. Parameter**
            
> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "",
        data: {
            id,
            code,
            name,
            name_kana,
            email,
            role,
            status,
            phone_number,
            is_you,
            branch: [
                {
                    id,
                    code,
                    name,
                    name_kana,
                    role
                }
            ],
            can_edit
        }
    }

- 403:
    {
        status_code: 403,
        message: "Permission denied.",
        errors: []
    }
                   
- 500:
    {
        status_code: 500,
        message: "Manager cannot be showed.",
        errors: []
    }
```

**6. Code handle**

- Controller - function:
	+ ManagerController@show
- Service:
- Repository:
    + App\Repositories\Eloquent\ManagerRepository
    
**7. Table get data**

- managers

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

### Create manager
**1. URL**

- POST: {URL}/managers

**2. Description**

- Create information of manager by admin
- Notice: Send email to manger that is created (company name, name, company code, manager code, password, url)

**3. Last modify**

	// TO DO

**4. Parameter**
       
- code:
    + required
    + max:255
    + type_code
    + unique:managers,code            
- name:
    + required
    + max:40            
- name_kana:
    + required
    + max:40
    + is_kana
    + fullsize
- email:
    + required
    + email
    + max:255
    + unique:managers,email
- password:
    + require_trim
    + required
    + type_password
    + min:6
    + max:255
- password_confirmation:
    + require_same:password
- phone_number:
    + phone_number
    + max:20
- roles:
    + required
            
> **5. Response**

```php
- 201:
    {
        status_code: 201,
        message: "Successfully added manager.",
        data: {
            id
        }
    }

- 403:
    {
        status_code: 403,
        message: "Permission denied.",
        errors: []
    }
                   
    
- 422:
    {
        status_code: 422,
        message: "Data validation failed.",
        errors: []
    }      
         
- 500:
    {
        status_code: 500,
        message: "Manager cannot be showed.",
        errors: []
    }
```

**6. Code handle**

- Controller - function:
	+ ManagerController@store
- Service:
- Repository:
    + App\Repositories\Eloquent\ManagerRepository
    
**7. Table get data**

- managers

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

### Update manager
**1. URL**

- PUT: {URL}/managers/{id}

**2. Description**

- Update information of manager by admin

**3. Last modify**

	// TO DO

**4. Parameter**
       
- code:
    + required
    + max:255
    + type_code
    + unique:managers,code            
- name:
    + required
    + max:40            
- name_kana:
    + required
    + max:40
    + is_kana
    + fullsize
- email:
    + required
    + email
    + max:255
    + unique:managers,email
- password:
    + require_trim
    + required
    + type_password
    + min:6
    + max:255
- password_confirmation:
    + require_same:password
- phone_number:
    + phone_number
    + max:20
- roles:
    + required
            
> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "Successfully updated manager.",
        data: []
    }
    
- 403:
    {
        status_code: 403,
        message: "Permission denied.",
        errors: []
    }
                   
- 422:
    {
        status_code: 422,
        message: "Data validation failed.",
        errors: []
    }      
         
- 500:
    {
        status_code: 500,
        message: "An error occurred while updating the manager.",
        errors: []
    }
```

**6. Code handle**

- Controller - function:
	+ ManagerController@update
- Service:
- Repository:
    + App\Repositories\Eloquent\ManagerRepository
    
**7. Table get data**

- managers

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

### Delete manager
**1. URL**

- DELETE: {URL}/managers/{id}

**2. Description**

- Delete manager by admin

**3. Last modify**

	// TO DO

**4. Parameter**
       
- current_password: 
    + required
    + current_password        
            
> **5. Response**

```php
- 201:
    {
        status_code: 201,
        message: "Successfully deleted manager.",
        data: []
    } 
         
- 403:
    {
        status_code: 403,
        message: "Permission denied.",
        errors: []
    }
                   
- 500:
    {
        status_code: 500,
        message: "An error occurred deleting the manager.",
        errors: []
    }
```

**6. Code handle**

- Controller - function:
	+ ManagerController@destroy
- Service:
- Repository:
    + App\Repositories\Eloquent\ManagerRepository
    
**7. Table get data**

- managers

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

### Show manager profile
**1. URL**

- DELETE: {URL}/managers/info/show

**2. Description**

- Show information of manager

**3. Last modify**

	// TO DO

**4. Parameter**
            
> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "",
        data: {
            id,
            code,
            email,
            name,
            name_kana,
            phone_number,
            enable_two_factor,
            is_super_admin
        }
    } 
         
- 500:
    {
        status_code: 500,
        message: "Occur error when showing your information.",
        errors: []
    }
```

**6. Code handle**

- Controller - function:
	+ ManagerController@showMyInfo
- Service:
- Repository:
    + App\Repositories\Eloquent\ManagerRepository
    
**7. Table get data**

- managers

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

### Update manager profile
**1. URL**

- PUT: {URL}/managers/info/update

**2. Description**

- Update information of manager

**3. Last modify**

	// TO DO

**4. Parameter**
       
- name:
    + required
    + max:40
- name_kana:
    + required
    + max:40
    + is_kana
    + fullsize
- email:
    + required
    + email
    + max:255
    + unique:managers,email
- phone_number:
    + phone_number
    + max:20

> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "Your information has been updated successfully.",
        data: {
            id,
            code,
            email,
            name,
            name_kana,
            phone_number,
            enable_two_factor,
            is_super_admin
        }
    } 
    
- 422:
    {
        status_code: 422,
        message: "Data validation failed.",
        errors: []
    }     
         
- 500:
    {
        status_code: 500,
        message: "Occur error when updating your information.",
        errors: []
    }
```

**6. Code handle**

- Controller - function:
	+ ManagerController@updateMyInfo
- Service:
- Repository:
    + App\Repositories\Eloquent\ManagerRepository
    
**7. Table get data**

- managers

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

### Change password current manager
**1. URL**

- PUT: {URL}/managers/info/change-password

**2. Description**

- Change password for manager

**3. Last modify**

	// TO DO

**4. Parameter**
       
- current_password:
    + require_trim
    + required
    + current_password
- password:
    + require_trim
    + required
    + type_password
    + min:6
    + max:255
    + different:current_password
- password_confirmation:
    + require_same:password
    
> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "Password has been updated successfully.",
        data: []
    } 
    
- 422:
    {
        status_code: 422,
        message: "Data validation failed.",
        errors: []
    }     
         
- 500:
    {
        status_code: 500,
        message: "Password cannot be updated.",
        errors: []
    }
```

**6. Code handle**

- Controller - function:
	+ ManagerController@changeMyPassword
- Service:
- Repository:
    + App\Repositories\Eloquent\ManagerRepository
    
**7. Table get data**

- managers

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

## Company
### Show infomation of company
**1. URL**

- GET: {URL}/companies/show

**2. Description**

- Show information of company

**3. Last modify**

	// TO DO

**4. Parameter**

> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "",
        data: {
            id,
            code,
            name,
            name_kana,
            zip_code,
            address1,
            address2,
            department_name_first_part,
            department_name_middle_part,
            department_name_last_part,
            prepaid_usage_list_type: {
                id,
                name
            },
            company_admin: {
                code,
                name,
                name_kana,
                email,
                company_id,
                phone_number
            },
            company_prepaid_setting: {
                company_id,
                upper_limit_rate,
                upper_limit_total_per_day,
                insurance_fee,
                consumption_tax_flag,
                is_missing_amount,
                prepayment_fee,
                system_using_fee,
                upper_limit_prepayment_fee
            },
            company_payment_cycle: {
                company_id,
                type,
                closing_kintai_day,
                start_applying_day,
                end_applying_day,
                start_applying_month,
                end_applying_month,
                closing_fee_day_after,
                paying_salary_day,
                paying_salary_month,
                end_applying_date_before,
                type_cycle_object: {
                    id,
                    name
                },
                start_applying_month_object: {
                    id,
                    name
                },
                end_applying_month_object: {
                    id,
                    name
                },
                paying_salary_month_object: {
                    id,
                    name
                }
            },
            company_bank_setting: {
                company_id,
                bank_code,
                bank_name,
                bank_branch_code,
                bank_branch_name,
                deposit_type: {
                    id,
                    name
                },
                bank_account_number,
                bank_account_holder,
                transfer_name,
                bank_start_time,
                init_fee,
                monthly_fee
            }
        }
    }

- 500:
    {
        
        status_code: 500,
        message: "Company information cannot be showed.",
        errors: []
    }
```

**6. Code handle**

- Controller - function:
	+ CompanyController@show
- Service:
- Repository:
    + App\Repositories\Eloquent\CompanyRepository
    
**7. Table get data**

- managers
- companies
- company_prepaid_setting
- company_payment_cycles
- company_bank_settings

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

### Update company info
**1. URL**

- PUT: {URL}/companies

**2. Description**

- Update information of company

**3. Last modify**

	// TO DO

**4. Parameter**

- name:
    + required
    + max:40
- name_kana:
    + required
    + is_kana
    + fullsize
    + max:40
- zip_code:
    + required
    + zip_code_number
    + fullsize
    + max:20
- address1:
    + required
    + max:255
- address2:
    + max:255
- department_name_first_part:
    + required
    + max:40
- department_name_middle_part:
    + nullable
    + max:40
- department_name_last_part:
    + nullable
    + max:40
- prepaid_usage_list_type:
    + required
    + in:Company::PREPAID_USAGE_RATE_NOT_SHOW,Company::PREPAID_USAGE_RATE_SHOW

> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "Company has been updated successfully.",
        data: []
    }

- 403:
    {
        
        status_code: 403,
        message: "Permission denied.",
        errors: []
    }

- 422:
    {
        status_code: 422,
        message: "Data validation failed.",
        errors: []
    }     
         
- 500:
    {
        
        status_code: 500,
        message: "Update company error.",
        errors: []
    }
```

**6. Code handle**

- Controller - function:
	+ CompanyController@update
- Service:
- Repository:
    + App\Repositories\Eloquent\CompanyRepository
    
**7. Table get data**

- companies

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

### Update payment cycle setting of company
**1. URL**

- PUT: {URL}/companies/payment-cycle

**2. Description**

- Update prepaid setting of company

**3. Last modify**

	// TO DO

**4. Parameter**

> **5. Response**

```php   
- 500:
    {
        
        status_code: 500,
        message: "An error occurred while updating the company cycle setting.",
        errors: []
    }
```

**6. Code handle**

- Controller - function:
    + CompanyController@updateCompanyPaymentCycle
- Service:
- Repository:
    
**7. Table get data**


**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

### Update prepaid setting of company
**1. URL**

- PUT: {URL}/companies/prepaid-setting

**2. Description**

- Update prepaid setting of company

**3. Last modify**

	// TO DO

**4. Parameter**

- upper_limit_rate:
    + required
    + integer
    + max:100
    + min: max_branch_upper_limit_rate
- upper_limit_total_per_day:
    + nullable
    + integer
    + min: max(max_branch_upper_limit_total_per_day, company_upper_limit_per_once)
- insurance_fee:
    + required
    + integer
    + min:max_branch_insurance_fee
    
> **5. Response**

```php  
- 200:
    {
        status_code: 200,
        message: "Successfully update prepaid setting.",
        data: []
    }
    
- 422:
    {
        status_code: 422,
        message: "Data validation failed",
        errors: []
    }
 
- 500:
    {
        
        status_code: 500,
        message: "An error occurred while updating the prepaid setting.",
        errors: []
    }
```

**6. Code handle**

- Controller - function:
    + CompanyController@updateCompanyPrepaidSetting
- Service:
- Repository:
    + App\Repositories\Eloquent\CompanyPrepaidSettingRepository
    
**7. Table get data**

- company_prepaid_setting

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

### Show current schedule of company
**1. URL**

- GET: {URL}/companies/schedules

**2. Description**

- Show detail of company payment schedule

**3. Last modify**

	// TO DO

**4. Parameter**

> **5. Response**

```php  
- 200:
    {
        status_code: 200,
        message: "",
        data:  {
            closing_kintai_time,
            start_applying_time,
            end_applying_time,
            closing_fee_time,
            paying_salary_time
        }
    }
    
- 500:
    {
        
        status_code: 500,
        message: "Company payment schedule cannot be showed.",
        errors: []
    }
```

**6. Code handle**

- Controller - function:
    + CompanyPaymentScheduleController@show
- Service:
- Repository:
    + App\Repositories\Eloquent\CompanyPaymentScheduleRepository
    
**7. Table get data**

- company_payment_schedules

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

## Branch
### List manager of branch role manager
**1. URL**

- GET: {URL}/branches/{id}/managers

**2. Description**

- List managers of branch role manager

**3. Last modify**

	// TO DO

**4. Parameter**

> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "",
        data:  [
            {
                id,
                code,
                name,
                name_kana,
                phone_number,
                email
            }
        ]
    }

- 403:
    {
        status_code: 403,
        message: "Permission denied.",
        errors: []
    }

- 500:
    {
        
        status_code: 500,
        message: "Manager cannot be listed.",
        errors: []
    }
```

**6. Code handle**

- Controller - function:
	+ ManagerController@listBranchManagers
- Service:
- Repository:
    + App\Repositories\Eloquent\ManagerRepository
    
**7. Table get data**

- managers

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

### List manager of branch role admin
**1. URL**

- GET: {URL}/branches/{id}/admins

**2. Description**

- List managers of branch role admin

**3. Last modify**

	// TO DO

**4. Parameter**

> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "",
        data:  [
            {
                id,
                code,
                name,
                name_kana,
                phone_number,
                email
            }
        ]
    }

- 403:
    {
        status_code: 403,
        message: "Permission denied.",
        errors: []
    }

- 500:
    {
        
        status_code: 500,
        message: "Manager cannot be listed.",
        errors: []
    }
```

**6. Code handle**

- Controller - function:
	+ ManagerController@listBranchAdmins
- Service:
- Repository:
    + App\Repositories\Eloquent\ManagerRepository
    
**7. Table get data**

- managers

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

### Delete branches
**1. URL**

- DELETE: {URL}/branches/deletes

**2. Description**

- Delete branches by id

**3. Last modify**

	// TO DO

**4. Parameter**

- ids
- current_password

> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "Branch has been deleted successfully.",
        data:  []
    }

- 403:
    {
        status_code: 403,
        message: "The current password does not match.",
        errors: []
    }
    
- 422:
    {
        status_code: 422,
        message: "Because of exist employees, you can not delete the branch.",
        errors: []
    }

- 500:
    {
        
        status_code: 500,
        message: "Branch delete fail",
        errors: []
    }
```

**6. Code handle**

- Controller - function:
	+ BranchController@deletes
- Service:
- Repository:
    + App\Repositories\Eloquent\ManagerRepository
    + App\Repositories\Eloquent\BranchRepository
    
**7. Table get data**

- managers

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

### Add branch manager
**1. URL**

- POST: {URL}/branches/{branchId}/add-branch-managers/{managerId}

**2. Description**

- Add branch manager

**3. Last modify**

	// TO DO

**4. Parameter**

- manager_id:
    + integer
    + exists:managers,id,company_id,$user->company_id,role,! Manager::IS_COMPANY_ADMIN,role,! Manager::IS_SUPER_ADMIN
    + nullable

> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "Successfully added person in charge.",
        data: {
            id,
            name,
            name_kana,
            code,
            email,
            phone_number
        }
    }

- 403:
    {
        status_code: 403,
        message: "Permission denied.",
        errors: []
    }
    
- 422:
    {
        status_code: 422,
        message: "Data validation failed",
        errors: []
    }

- 500:
    {
        
        status_code: 500,
        message: "Branch manager cannot be created.",
        errors: []
    }
```

**6. Code handle**

- Controller - function:
	+ BranchManagerController@addBranchManagerId
- Service:
- Repository:
    + App\Repositories\Eloquent\BranchManagerRepository
    
**7. Table get data**

- managers
- branch_manager

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

### Detach branch manager
**1. URL**

- POST: {URL}/branches/{branchId}/detach-branch-managers/{managerId}

**2. Description**

- Detach manager out branch

**3. Last modify**

	// TO DO

**4. Parameter**

> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "Successfully deleted the person in charge",
        data: []
    }

- 403:
    {
        status_code: 403,
        message: "Permission denied.",
        errors: []
    }

- 500:
    {
        
        status_code: 500,
        message: "Failed to delete person in charge.",
        errors: []
    }
```

**6. Code handle**

- Controller - function:
	+ BranchManagerController@detachBranchManager
- Service:
    + App\Services\BranchManagers\DetachService
- Repository:
    + App\Repositories\Eloquent\BranchManagerRepository
    + App\Repositories\Eloquent\BranchRepository
    + App\Repositories\Eloquent\ManagerRepository
    
**7. Table get data**

- managers
- branch_manager

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

### Show branch admin
**1. URL**

- GET: {URL}/branches/{branchId}/branch-admin/{managerId}

**2. Description**

- Show branch admin

**3. Last modify**

	// TO DO

**4. Parameter**

- id

> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "",
        data: {
            id
            code
            email
            name
            name_kana
            role
            status
            phone_number
            created_at
        }
    }

- 403:
    {
        status_code: 403,
        message: "Permission denied.",
        errors: []
    }

- 500:
    {
        
        status_code: 500,
        message: "Branch admin cannot be listed.",
        errors: []
    }
```

**6. Code handle**

- Controller - function:
	+ BranchAdminController@show
- Service:
- Repository:
    + App\Repositories\Eloquent\BranchAdminRepository
    
**7. Table get data**

- managers

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

### Create branch admin.
**1. URL**

- POST: {URL}/branches/{id}/branch-admin

**2. Description**

- Create branch admin.
- Notice: Send email to manger that is created (company name, manager name, company code, manager code, password, url)

**3. Last modify**

	// TO DO

**4. Parameter**

- code:
    + required
    + max:255
    + type_code
    + unique:managers,code
- name:
    + max:40
    + required
- name_kana:
    + max:40
    + is_kana
    + fullsize
    + required
- email:
    + required
    + email
    + max:255
    + unique:managers,email
- phone_number:
    + nullable
    + phone_number
    + max:20
- password:
    + require_trim
    + min:6
    + max:255
    + required
    + type_password
- password_confirmation:
    + require_same:password

> **5. Response**

```php
- 201:
    {
        status_code: 201,
        message: "Successfully added administrator.",
        data: {
            id
            name
            name_kana
            code
            email
            phone_number
        }
    }

- 403:
    {
        status_code: 403,
        message: "Permission denied.",
        errors: []
    }

- 422:
    {
        status_code: 422,
        message: "Data validation failed",
        errors: []
    }

- 500:
    {
        
        status_code: 500,
        message: "There was an error adding the administrator.",
        errors: []
    }
```

**6. Code handle**

- Controller - function:
	+ BranchAdminController@store
- Service:
- Repository:
    + App\Repositories\Eloquent\BranchAdminRepository
    + App\Repositories\Eloquent\ManagerRepository
    
**7. Table get data**

- managers

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

### Update branch admin.
**1. URL**

- POST: {URL}/branches/{id}/branch-admin/{id}

**2. Description**

- Update branch admin.

**3. Last modify**

	// TO DO

**4. Parameter**

- code:
    + required
    + max:255
    + type_code
    + unique:managers,code
- name:
    + max:40
    + required
- name_kana:
    + max:40
    + is_kana
    + fullsize
    + required
- email:
    + required
    + email
    + max:255
    + unique:managers,email
- phone_number:
    + nullable
    + phone_number
    + max:20
- password:
    + require_trim
    + min:6
    + max:255
    + required
    + type_password
- password_confirmation:
    + require_same:password
    
> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "Successfully updated administrator.",
        data: {
            id
            name
            name_kana
            code
            email
            phone_number
            is_you
        }
    }

- 403:
    {
        status_code: 403,
        message: "Permission denied.",
        errors: []
    }
    
- 422:
    {
        status_code: 422,
        message: "Data validation failed",
        errors: []
    }

- 500:
    {
        
        status_code: 500,
        message: "There was an error updating the administrator.",
        errors: []
    }
```

**6. Code handle**

- Controller - function:
	+ BranchAdminController@update
- Service:
- Repository:
    + App\Repositories\Eloquent\ManagerRepository
    
**7. Table get data**

- managers

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

### Delete branch admin.
**1. URL**

- DELETE: {URL}/branches/{id}/branch-admin/{id}

**2. Description**

- Delete branch admin.

**3. Last modify**

	// TO DO

**4. Parameter**

- current_password:
    + required
    + current_password

> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "Successfully deleted administrator.",
        data: []
    }

- 403:
    {
        status_code: 403,
        message: "Permission denied.",
        errors: []
    }
    
- 422:
    {
        status_code: 422,
        message: "Data validation failed",
        errors: []
    }

- 500:
    {
        
        status_code: 500,
        message: "An error occurred deleting the administrator.",
        errors: []
    }
```

**6. Code handle**

- Controller - function:
	+ BranchAdminController@update
- Service:
- Repository:
    + App\Repositories\Eloquent\ManagerRepository
    
**7. Table get data**

- managers
- branch_manager

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

### Add branch admin
**1. URL**

- POST: {URL}/branches/{branchId}/add-branch-admin/{managerId}

**2. Description**

- Add branch admin.

**3. Last modify**

	// TO DO

**4. Parameter**

> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "Successfully deleted administrator.",
        data: {
            id
            name
            name_kana
            code
            email
            phone_number
        }
    }

- 403:
    {
        status_code: 403,
        message: "Permission denied.",
        errors: []
    }
    
- 422:
    {
        status_code: 422,
        message: "Data validation failed",
        errors: []
    }

- 500:
    {
        
        status_code: 500,
        message: "Branch admin cannot be created.",
        errors: []
    }
```

**6. Code handle**

- Controller - function:
	+ BranchAdminController@addBranchAdminId
- Service:
- Repository:
    + App\Repositories\Eloquent\BranchAdminRepository
    
**7. Table get data**

- managers
- branch_manager

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

### Detach branch admin
**1. URL**

- POST: {URL}/branches/{branchId}/detach-branch-admin/{managerId}

**2. Description**

- Detach manager out branch.

**3. Last modify**

	// TO DO

**4. Parameter**

> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "Successfully deleted administrator.",
        data: []
    }
    
- 403:
    {
        status_code: 403,
        message: "Permission denied.",
        errors: []
    }

- 422:
    {
        status_code: 422,
        message: "Data validation failed",
        errors: []
    }

- 500:
    {
        
        status_code: 500,
        message: "Failed to delete administrator.",
        errors: []
    }
```

**6. Code handle**

- Controller - function:
	+ BranchAdminController@detachBranchAdmin
- Service:
- Repository:
    + App\Repositories\Eloquent\BranchRepository
    + App\Repositories\Eloquent\ManagerRepository
    
**7. Table get data**

- managers
- branch_manager

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

### Show branch manager.
**1. URL**

- POST: {URL}/branches/{id}/branch-managers/{managerId}

**2. Description**

- Show branch manager.

**3. Last modify**

	// TO DO

**4. Parameter**

> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "",
        data: {
            id
            code
            email
            name
            name_kana
            role
            status
            phone_number
            created_at
        }
    }
    
- 422:
    {
        status_code: 422,
        message: "Data validation failed",
        errors: []
    }

- 500:
    {
        
        status_code: 500,
        message: "Branch manager cannot be showed.",
        errors: []
    }
```

**6. Code handle**

- Controller - function:
	+ BranchManagerController@show
- Service:
- Repository:
    + App\Repositories\Eloquent\BranchManagerRepository
    
**7. Table get data**

- managers

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

### Create branch manager.
**1. URL**

- POST: {URL}/branches/{id}/branch-managers/{managerId}

**2. Description**

- Create branch manager
- Notice: Send email to manger that is created

**3. Last modify**

	// TO DO

**4. Parameter**

- code:
    + required
    + max:255
    + type_code
    + unique:managers,code
- name:
    + max:40
    + required
- name_kana:
    + max:40
    + is_kana
    + fullsize
    + required
- email:
    + required
    + email
    + max:255
    + unique:managers,email
- phone_number:
    + nullable
    + phone_number
    + max:20
- password:
    + require_trim
    + min:6
    + max:255
    + required
    + type_password
- password_confirmation:
    + require_same:password
    
> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "Successfully added person in charge.",
        data: {
            name,
            name_kana,
            code,
            email,
            phone_number,
            id
        }
    }
    
- 403:
    {
        status_code: 403,
        message: "Permission denied.",
        errors: []
    }
    
- 422:
    {
        status_code: 422,
        message: "Data validation failed",
        errors: []
    }

- 500:
    {
        
        status_code: 500,
        message: "Error adding administrator.",
        errors: []
    }
```

**6. Code handle**

- Controller - function:
	+ BranchManagerController@store
- Service:
- Repository:
    + App\Repositories\Eloquent\BranchManagerRepository
    
**7. Table get data**

- managers

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

### Update branch manager.
**1. URL**

- POST: {URL}/branches/{id}/branch-managers/{managerId}

**2. Description**

- Create branch manager

**3. Last modify**

	// TO DO

**4. Parameter**

- code:
    + required
    + max:255
    + type_code
    + unique:managers,code
- name:
    + max:40
    + required
- name_kana:
    + max:40
    + is_kana
    + fullsize
    + required
- email:
    + required
    + email
    + max:255
    + unique:managers,email
- phone_number:
    + nullable
    + phone_number
    + max:20
- password:
    + require_trim
    + min:6
    + max:255
    + required
    + type_password
- password_confirmation:
    + require_same:password

> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "Successfully updated person in charge.",
        data: {
            name,
            name_kana,
            code,
            email,
            phone_number,
            id
        }
    }
    
- 403:
    {
        status_code: 403,
        message: "Permission denied.",
        errors: []
    }
    
- 422:
    {
        status_code: 422,
        message: "Data validation failed",
        errors: []
    }

- 500:
    {
        
        status_code: 500,
        message: "An error occurred while updating the contact person.",
        errors: []
    }
```

**6. Code handle**

- Controller - function:
	+ BranchManagerController@update
- Service:
- Repository:
    + App\Repositories\Eloquent\ManagerRepository
    
**7. Table get data**

- managers

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

### Delete branch manager
**1. URL**

- DELETE: {URL}/branches/{id}/branch-managers/{managerId}

**2. Description**

- Delete branch manager

**3. Last modify**

	// TO DO

**4. Parameter**

- current_password

> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "Successfully deleted person in charge.",
        data: []
    }
    
- 403:
    {
        status_code: 403,
        message: "The current password does not match.",
        errors: []
    }
    
- 500:
    {
        
        status_code: 500,
        message: "An error occurred deleting the contact person.",
        errors: []
    }
```

**6. Code handle**

- Controller - function:
	+ BranchManagerController@destroy
- Service:
- Repository:
    + App\Repositories\Eloquent\ManagerRepository
    
**7. Table get data**

- managers
- branch_manager

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

### List branches role admin
**1. URL**

- GET: {URL}/branches/admins

**2. Description**

- List branches role admin

**3. Last modify**

	// TO DO

**4. Parameter**

> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "",
        data: [
            {
                id
                name
                role
            }
        ]
    }
    
- 500:
    {
        status_code: 500,
        message: "Branch cannot be listed.",
        errors: []
    }
```

**6. Code handle**

- Controller - function:
	+ BranchController@listBranchesRoleAdmin
- Service:
- Repository:
    + App\Repositories\Eloquent\BranchRepository
    
**7. Table get data**

- managers

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

### List branches
**1. URL**

- GET: {URL}/branches

**2. Description**

- List branches

**3. Last modify**

	// TO DO

**4. Parameter**

> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "",
        data: {
            total
            per_page
            current_page
            last_page
            next_page_url
            prev_page_url
            from
            to
            items: [
                {
                    id
                    code
                    name
                    name_kana
                    zip_code
                    company_id
                    address
                    phone
                    fax
                    manager_id
                    manager_name
                    manager_name_kana
                    manager_email
                    created_at
                    can_edit
                    can_delete
                }
            ]
        }
    }
    
- 500:
    {
        
        status_code: 500,
        message: "Branch cannot be listed.",
        errors: []
    }
```

**6. Code handle**

- Controller - function:
	+ BranchController@index
- Service:
- Repository:
    + App\Repositories\Eloquent\BranchRepository
    
**7. Table get data**

- branches

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

### Create branch
**1. URL**

- POST: {URL}/branches

**2. Description**

- Create a branch

**3. Last modify**

	// TO DO

**4. Parameter**

- code:
    + required
    + type_code
    + unique:branches,code
    + max:255
- name:
    + required
    + unique:branches,name
- name_kana:
    + is_kana
    + fullsize
    + max:40
- zip_code:
    + zip_code_number
    + fullsize
    + max:20
- address:
    + zip_code_number|fullsize|max:20
- phone:
    + phone_number
    + max:20
- fax:
    + fax_number
    + max:20
- manager_code:
    + max:255
    + type_code
    + unique:managers,code
    + required_with:manager_name,manager_name_kana,manager_email,manager_password,manager_password_confirmation
- manager_name:
    + max:40
    + required_with:manager_code,manager_name_kana,manager_email,manager_password,manager_password_confirmation
- manager_name_kana:
    + is_kana
    + max:40
    + required_with:manager_code,manager_name,manager_email,manager_password,manager_password_confirmation
- manager_email:
    + email
    + max:255
    + unique:managers,email
    + required_with:manager_code,manager_name,manager_name_kana,manager_password,manager_password_confirmation
- manager_phone_number:
    + nullable
    + phone_number
    + max:20
- manager_password:
    + require_trim
    + min:6
    + max:255
    + required_with:manager_code,manager_name,manager_name_kana,manager_email
- manager_password_confirmation:
    + require_same:manager_password

> **5. Response**

```php
- 201:
    {
        status_code: 201,
        message: "Branch has been added successfully.",
        data: {
            id
        }
    }

- 422:
    {
        
        status_code: 422,
        message: "Data validation failed",
        errors: []
    }
    
- 500:
    {
        
        status_code: 500,
        message: "Create branch error.",
        errors: []
    }
```

**6. Code handle**

- Controller - function:
	+ BranchController@store
- Service:
    + App\Services\Branches\AddBranchService
- Repository:
    + App\Repositories\Eloquent\BranchRepository
    + App\Repositories\Eloquent\BranchAdminRepository
    + App\Repositories\Eloquent\BranchPrepaidSettingRepository
    + App\Repositories\Eloquent\CompanyPaymentScheduleRepository
    + App\Repositories\Eloquent\BranchPrepaidScheduleRepository
    + App\Repositories\Eloquent\ManagerRepository
    
**7. Table get data**

- branches

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

### Update branch
**1. URL**

- PUT: {URL}/branches

**2. Description**

- Update branch info

**3. Last modify**

	// TO DO

**4. Parameter**

- code:
    + required
    + type_code
    + unique:branches,code
    + max:255
- name:
    + required
    + unique:branches,name
- name_kana:
    + is_kana
    + fullsize
    + max:40
- zip_code:
    + zip_code_number
    + fullsize
    + max:20
- address:
    + zip_code_number|fullsize|max:20
- phone:
    + phone_number
    + max:20
- fax:
    + fax_number
    + max:20
    
> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "Branch has been updated successfully.",
        data: []
    }

- 422:
    {
        
        status_code: 422,
        message: "Data validation failed",
        errors: []
    }
    
- 500:
    {
        
        status_code: 500,
        message: "Update create error",
        errors: []
    }
```

**6. Code handle**

- Controller - function:
	+ BranchController@update
- Service:
- Repository:
    + App\Repositories\Eloquent\BranchRepository
    
**7. Table get data**

- branches

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

### Show detail of branch
**1. URL**

- PUT: {URL}/branches

**2. Description**

- Show detail of branch

**3. Last modify**

	// TO DO

**4. Parameter**

> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "",
        data: {
            id',
            code
            name
            name_kana
            zip_code
            company_id
            address
            phone
            fax
            created_at
        }
    }
    
- 500:
    {
        
        status_code: 500,
        message: "Branch cannot be showed.",
        errors: []
    }
```

**6. Code handle**

- Controller - function:
	+ BranchController@show
- Service:
- Repository:
    + App\Repositories\Eloquent\BranchRepository
    
**7. Table get data**

- branches

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

### List managers of branch
**1. URL**

- GET: {URL}/branches/{branchId}/all-managers

**2. Description**

- List managers of branch

**3. Last modify**

	// TO DO

**4. Parameter**

> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "",
        data: {
            branch_admins: [
                {
                    id
                    code
                    email
                    name
                    name_kana
                    phone_number
                    is_you
                }
            ],
            branch_managers: [
                {
                    id
                    code
                    email
                    name
                    name_kana
                    phone_number
                    is_you
                }
            ]
        }
    }
    
- 500:
    {
        
        status_code: 500,
        message: "Manager cannot be listed.",
        errors: []
    }
```

**6. Code handle**

- Controller - function:
	+ BranchController@listManagersOfBranch
- Service:
- Repository:
    + App\Repositories\Eloquent\ManagerRepository
    
**7. Table get data**

- managers

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

### Show prepaid setting of branch
**1. URL**

- GET: {URL}/branches/{branchId}/prepaid-setting

**2. Description**

- Show prepaid setting of branch

**3. Last modify**

	// TO DO

**4. Parameter**

> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "",
        data: {
            upper_limit_rate
            upper_limit_total_per_day
            insurance_fee
            consumption_tax_flag
            system_using_fee
        }
    }
    
- 500:
    {
        
        status_code: 500,
        message: "Prepaid setting of branch cannot be showed.",
        errors: []
    }
```

**6. Code handle**

- Controller - function:
	+ BranchController@showBranchPrepaidSetting
- Service:
- Repository:
    + App\Repositories\Eloquent\BranchPrepaidSettingRepository
    
**7. Table get data**

- branch_prepaid_settings

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

### Update prepaid setting of branch
**1. URL**

- PUT: {URL}/branches/{branchId}/prepaid-setting

**2. Description**

- Update prepaid setting

**3. Last modify**

	// TO DO

**4. Parameter**

- upper_limit_rate:
    + required
    + integer
    + min:1
    + max:company_upper_limit_rate
- upper_limit_total_per_day:
    + nullable
    + integer
    + min:company_lower_limit_per_once 
    + max:company_upper_limit_total_per_day
- insurance_fee:
    + required
    + integer
    + min:0
    + max:company_insurance_fee

> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "Successfully update prepaid setting of branch.",
        data: []
    }

- 422:
    {
        
        status_code: 422,
        message: "Data validation failed",
        errors: []
    }
    
- 500:
    {
        
        status_code: 500,
        message: "An error occurred while updating the prepaid setting of branch.",
        errors: []
    }
```

**6. Code handle**

- Controller - function:
	+ BranchController@updateBranchPrepaidSetting
- Service:
- Repository:
    + App\Repositories\Eloquent\BranchRepository
    + App\Repositories\Eloquent\BranchPrepaidSettingRepository
    
**7. Table get data**

- branch_prepaid_settings

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

### Show prepaid schedule of branch
**1. URL**

- PUT: {URL}/branches/{branchId}/prepaid-schedule

**2. Description**

- Show prepaid schedule of branch

**3. Last modify**

	// TO DO

**4. Parameter**

> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "",
        data: {
            upper_limit_total_per_day
            upper_limit_rate
            insurance_fee
        }
    }

- 422:
    {
        
        status_code: 422,
        message: "Data validation failed",
        errors: []
    }
    
- 500:
    {
        
        status_code: 500,
        message: "Prepaid setting of branch cannot be showed.",
        errors: []
    }
```

**6. Code handle**

- Controller - function:
	+ BranchController@showBranchPrepaidSchedule
- Service:
- Repository:
    + App\Repositories\Eloquent\BranchRepository
    + App\Repositories\Eloquent\BranchPrepaidScheduleRepository
    + App\Repositories\Eloquent\CompanyPaymentScheduleRepository
    
**7. Table get data**

- branch_prepaid_settings

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

## Import
### Import employees
**1. URL**

- POST: {URL}/employees-csv/imports

**2. Description**

- Import employee csv

**3. Last modify**

	// TO DO

**4. Parameter**

- reason:
- reason_detail:
- stopped_at:
- action:

> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "CSV file is processing",
        data: []
    }

- 403:
    {
        status_code: 403,
        message: "Permission denied.",
        errors: []
    }

- 500:
    {
        
        status_code: 500,
        message: "CSV file import failed",
        errors: []
    }
```

**6. Code handle**

- Controller - function:
	+ ImportEmployeeController@import
- Service:
    + App\Services\ImportEmployee\EmployeeStopService
    + App\Services\ImportEmployee\EmployeeService
    + App\Services\ImportEmployee\CommonService
- Repository:
    + App\Repositories\Eloquent\ImportEmployeeStopServiceRepository
    + App\Repositories\Eloquent\ImportEmployeeRepository
    + App\Repositories\Eloquent\ManagerRepository
    
**7. Table get data**

- imports

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

### Listen process of import
**1. URL**

- GET: {URL}/imports/messages

**2. Description**

- Check status import

**3. Last modify**

	// TO DO

**4. Parameter**

> **5. Response**

```php
\Symfony\Component\HttpFoundation\StreamedResponse
```

**6. Code handle**

- Controller - function:
	+ ImportEmployeeController@messages
- Service:
- Repository:
    + App\Repositories\Eloquent\ImportEmployeeRepository
    
**7. Table get data**

- imports

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

### Download csv employee import
**1. URL**

- GET: {URL}/imports/download/{id}

**2. Description**

- Download employee CSV import file

**3. Last modify**

	// TO DO

**4. Parameter**

> **5. Response**

```php
- 200:
    Returns the number of characters read from the handle and passed through to the output.

- 500:
    {
        
        status_code: 500,
        message: "CSV imported file cannot be download.",
        errors: []
    }
```

**6. Code handle**

- Controller - function:
	+ ImportEmployeeController@download
- Service:
- Repository:
    + App\Repositories\Eloquent\ImportEmployeeRepository
    
**7. Table get data**

- imports

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

### Show csv employee import
**1. URL**

- GET: {URL}/imports/employee-csv/{id}

**2. Description**

- Show content of employee CSV import file

**3. Last modify**

	// TO DO

**4. Parameter**

- page: nullable
- per_page: nullable

> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "",
        data: {
            total
            per_page
            current_page
            last_page
            next_page_url
            prev_page_url
            from
            to
            items: [
                {
                    data record
                }
            ]
            headers: {}
        }
    }

- 500:
    {
        
        status_code: 500,
        message: "Content of csv file cannot be showed.",
        errors: []
    }
```

**6. Code handle**

- Controller - function:
	+ ImportEmployeeController@showCsv
- Service:
- Repository:
    + App\Repositories\Eloquent\ImportEmployeeRepository
    
**7. Table get data**

- imports

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

### List employee CSV import files
**1. URL**

- GET: {URL}/imports

**2. Description**

- List employee CSV import files

**3. Last modify**

	// TO DO

**4. Parameter**

- sort_column: nullable
- sort_direction: nullable
- per_page: nullable

> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "",
        data: {
            total
            per_page
            current_page
            last_page
            next_page_url
            prev_page_url
            from
            to
            items: [
                {
                    id
                    start_processing_time
                    stop_processing_time
                    filename
                    url_store
                    status
                    extension
                    number_records
                    manager_id
                    branch_ids
                    manager: {
                        id
                        code
                        name
                        name_kana
                    }
                }
            ]
        }
    }

- 500:
    {
        
        status_code: 500,
        message: "Imported files cannot be listed.",
        errors: []
    }
```

**6. Code handle**

- Controller - function:
	+ ImportEmployeeController@index
- Service:
- Repository:
    + App\Repositories\Eloquent\ImportEmployeeRepository
    
**7. Table get data**

- imports

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

### Show detail of employee CSV import file
**1. URL**

- GET: {URL}/imports/{id}

**2. Description**

- Show detail of employee CSV import file

**3. Last modify**

	// TO DO

**4. Parameter**

- id
- per_page

> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "",
        data: {
            total
            per_page
            current_page
            last_page
            next_page_url
            prev_page_url
            from
            to
            items: [
                {
                    id
                    import_id
                    row_error_number
                    row_error_content: {},
                    employee_code
                }
            ],
            import_info: {
                id
                start_processing_time
                stop_processing_time
                filename
                url_store
                status
                extension
                manager_id
                number_records
                warning_records
                normal_records
                manager: {
                    id
                    code
                    name
                    name_kana
                }
            }
        }
    }

- 500:
    {
        status_code: 500,
        message: "Imported row errors cannot be showed.",
        errors: []
    }
```

**6. Code handle**

- Controller - function:
	+ ImportEmployeeController@show
- Service:
- Repository:
    + App\Repositories\Eloquent\ImportEmployeeRepository
    
**7. Table get data**

- imports

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

### Delete employee CSV import file
**1. URL**

- DELETE: {URL}/imports/{id}

**2. Description**

- Delete employee CSV import file

**3. Last modify**

	// TO DO

**4. Parameter**

> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "CSV imported file has been deleted successfully.",
        data: []
        }
    }

- 500:
    {
        status_code: 500,
        message: "CSV imported file has been delete fail",
        errors: []
    }
```

**6. Code handle**

- Controller - function:
	+ ImportEmployeeController@destroy
- Service:
- Repository:
    + App\Repositories\Eloquent\ImportEmployeeRepository
    
**7. Table get data**

- imports

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

### Import employee kintai
**1. URL**

- POST: {URL}/kintais-csv/imports

**2. Description**

- Import employee kintai csv

**3. Last modify**

	// TO DO

**4. Parameter**

> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "CSV kintai file is processing",
        data: []
        }
    }

- 422:
    {
        status_code: 422,
        message: "Data validation failed",
        data: []
        }
    }

- 500:
    {
        
        status_code: 500,
        message: "CSV kintai file import failed",
        errors: []
    }
```

**6. Code handle**

- Controller - function:
	+ ImportEmployeeKintaiController@import
- Service:
    + App\Services\ImportEmployee\EmployeeKintaiService
- Repository:
    + App\Repositories\Eloquent\ImportEmployeeKintaiRepository
    
**7. Table get data**

- imports

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

### Download csv employee kintai import
**1. URL**

- GET: {URL}/import-kintais/download/{id}

**2. Description**

- Download kintai CSV import file

**3. Last modify**

	// TO DO

**4. Parameter**

> **5. Response**

```php
- 200:
    Returns the number of characters read from the handle and passed through to the output.

- 500:
    {
        
        status_code: 500,
        message: "CSV kintai file import failed",
        errors: []
    }
```

**6. Code handle**

- Controller - function:
	+ ImportEmployeeKintaiController@download
- Service:
- Repository:
    + App\Repositories\Eloquent\ImportEmployeeKintaiRepository
    
**7. Table get data**

- imports

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

### Show csv employee kintai
**1. URL**

- GET: {URL}/import-kintais/employee-csv/{id}

**2. Description**

- Show content of kintai CSV import file

**3. Last modify**

	// TO DO

**4. Parameter**

- page: nullable
- per_page: nullable

> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "",
        data: {
            total
            per_page
            current_page
            last_page
            next_page_url
            prev_page_url
            from
            to
            items: [
                {
                    data record
                }
            ]
            headers: {}
        }
    }

- 500:
    {
        status_code: 500,
        message: "Content of csv file cannot be showed.",
        errors: []
    }
```

**6. Code handle**

- Controller - function:
	+ ImportEmployeeKintaiController@showCsv
- Service:
- Repository:
    + App\Repositories\Eloquent\ImportEmployeeKintaiRepository
    
**7. Table get data**

- imports

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

### List kintai CSV import files
**1. URL**

- GET: {URL}/import-kintais

**2. Description**

- List kintai CSV import files

**3. Last modify**

	// TO DO

**4. Parameter**

- sort_column: nullable
- sort_direction: nullable
- per_page: nullable

> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "",
        data: {
            total
            per_page
            current_page
            last_page
            next_page_url
            prev_page_url
            from
            to
            items: [
                {
                    id
                    start_processing_time
                    stop_processing_time
                    filename
                    url_store
                    status
                    extension
                    number_records
                    manager_id
                    branch_ids
                    manager: {
                        id
                        code
                        name
                        name_kana
                    }
                }
            ]
        }
    }

- 500:
    {
        status_code: 500,
        message: "Imported files cannot be listed.",
        errors: []
    }
```

**6. Code handle**

- Controller - function:
	+ ImportEmployeeKintaiController@index
- Service:
- Repository:
    + App\Repositories\Eloquent\ImportEmployeeKintaiRepository
    
**7. Table get data**

- imports

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

### Show detail of kintai CSV import file
**1. URL**

- GET: {URL}/import-kintais/{id}

**2. Description**

- Show detail of kintai CSV import file

**3. Last modify**

	// TO DO

**4. Parameter**

- id
- per_page

> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "",
        data: {
            total
            per_page
            current_page
            last_page
            next_page_url
            prev_page_url
            from
            to
            items: [
                {
                    id
                    import_id
                    row_error_number
                    row_error_content: {},
                    employee_code
                }
            ],
            import_info: {
                id
                start_processing_time
                stop_processing_time
                filename
                url_store
                status
                extension
                manager_id
                number_records
                warning_records
                normal_records
                manager: {
                    id
                    code
                    name
                    name_kana
                }
            }
        }
    }

- 500:
    {
        status_code: 500,
        message: "Imported row errors cannot be showed.",
        errors: []
    }
```

**6. Code handle**

- Controller - function:
	+ ImportEmployeeKintaiController@show
- Service:
- Repository:
    + App\Repositories\Eloquent\ImportEmployeeKintaiRepository
    
**7. Table get data**

- imports

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

### Delete kintai CSV import file
**1. URL**

- DELETE: {URL}/imports/{id}

**2. Description**

- Delete kintai CSV import file

**3. Last modify**

	// TO DO

**4. Parameter**

> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "CSV imported file has been deleted successfully.",
        data: []
        }
    }

- 500:
    {
        status_code: 500,
        message: "CSV imported file has been delete fail",
        errors: []
    }
```

**6. Code handle**

- Controller - function:
	+ ImportEmployeeKintaiController@destroy
- Service:
- Repository:
    + App\Repositories\Eloquent\ImportEmployeeKintaiRepository
    
**7. Table get data**

- imports

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

## Mail template
### Get replacement tag
**1. URL**

- GET: {URL}/mail-templates/replace-tag

**2. Description**

- Get replacement tag.

**3. Last modify**

	// TO DO

**4. Parameter**

> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "",
        data:  [
            {
                id
                name
            }
        ]
    }
```

**6. Code handle**

- Controller - function:
	+ MailTemplateController@getReplacementTag
- Service:
- Repository:
    + App\Repositories\Eloquent\MailTemplateRepository
    
**7. Table get data**

- mail_templates

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

### List mail templates.
**1. URL**

- GET: {URL}/mail-templates

**2. Description**

- List mail templates.

**3. Last modify**

	// TO DO

**4. Parameter**

- sort_column: nullable
- sort_direction: nullable

> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "",
        data: {
            total
            per_page
            current_page
            last_page
            next_page_url
            prev_page_url
            from
            to
            items: [
                {
                    id
                    title
                    receiver_type
                    count_sent
                    last_sent_at
                    created_at
                    manager_id
                    manager: {
                        id
                        name
                        name_kana
                    }
                },
            ]
        }
    }
    
- 200:
    {
        status_code: 200,
        message: "Occur error while listing mail templates",
        errors:  []
    }    
```

**6. Code handle**

- Controller - function:
	+ MailTemplateController@index
- Service:
- Repository:
    + App\Repositories\Eloquent\MailTemplateRepository
    
**7. Table get data**

- mail_templates

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

### Create a mail template.
**1. URL**

- POST: {URL}/mail-templates

**2. Description**

- Create a mail template.

**3. Last modify**

	// TO DO

**4. Parameter**

- title:
    + required
    + unique:mail_templates,title,NULL,id,company_id,$user->company_id 
    + max:255
- content:
    + required
- receiver_type:
    + required
    + in:MailTemplate::SEND_TO_EMPLOYEE,MailTemplate::SEND_TO_MANAGER

> **5. Response**

```php
- 201:
    {
        status_code: 201,
        message: "Mail template has been created successfully.",
        data: {
            id
        }
    }

- 422:
    {
        status_code: 422,
        message: "Data validation failed",
        errors:  []
    }   
    
- 500:
    {
        status_code: 500,
        message: "Occur error while creating mail template.",
        errors:  []
    }    
```

**6. Code handle**

- Controller - function:
	+ MailTemplateController@store
- Service:
- Repository:
    + App\Repositories\Eloquent\MailTemplateRepository
    
**7. Table get data**

- mail_templates

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

### Update a mail template.
**1. URL**

- PUT: {URL}/mail-templates/{id}

**2. Description**

- Update a mail template.

**3. Last modify**

	// TO DO

**4. Parameter**

- title:
    + required
    + unique:mail_templates,title,NULL,id,company_id,$user->company_id 
    + max:255
- content:
    + required
- receiver_type:
    + required
    + in:MailTemplate::SEND_TO_EMPLOYEE,MailTemplate::SEND_TO_MANAGER

> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "Mail template has been updated successfully.",
        data: []
    }

- 422:
    {
        status_code: 422,
        message: "Data validation failed",
        errors:  []
    }   
    
- 500:
    {
        status_code: 500,
        message: "Occur error while updating mail template.",
        errors:  []
    }    
```

**6. Code handle**

- Controller - function:
	+ MailTemplateController@update
- Service:
- Repository:
    + App\Repositories\Eloquent\MailTemplateRepository
    
**7. Table get data**

- mail_templates

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

### Show a mail template.
**1. URL**

- GET: {URL}/mail-templates/{id}

**2. Description**

- Show a mail template.

**3. Last modify**

	// TO DO

**4. Parameter**

> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "",
        data:  {
            id
            title
            content
            receiver_type
            count_sent
            last_sent_at
            created_at
            manager_id
            manager: {
                id
                name
                name_kana
            }
        }
    } 
    
- 500:
    {
        status_code: 500,
        message: "Occur error while showing mail template.",
        errors:  []
    }    
```

**6. Code handle**

- Controller - function:
	+ MailTemplateController@show
- Service:
- Repository:
    + App\Repositories\Eloquent\MailTemplateRepository
    
**7. Table get data**

- mail_templates

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

### Delete a mail template.
**1. URL**

- DELETE: {URL}/mail-templates/{id}

**2. Description**

- Delete a mail template.

**3. Last modify**

	// TO DO

**4. Parameter**

> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "Mail template has been deleted successfully.",
        data:  []
    } 
    
- 500:
    {
        status_code: 500,
        message: "Occur error while deleting mail template.",
        errors:  []
    }    
```

**6. Code handle**

- Controller - function:
	+ MailTemplateController@destroy
- Service:
- Repository:
    + App\Repositories\Eloquent\MailTemplateRepository
    
**7. Table get data**

- mail_templates

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

### Send email
**1. URL**

- POST: {URL}/mail-templates/send/{id}

**2. Description**

- Send email.

**3. Last modify**

	// TO DO

**4. Parameter**

- id
 
> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "Send email successfully."
        data:  []
    } 
    
- 500:
    {
        status_code: 500,
        message: "Send email fail.",
        errors:  []
    }    
```

**6. Code handle**

- Controller - function:
	+ MailTemplateController@send
- Service:
    + App\Services\MailTemplate\SendMailTemplateService
    + App\Services\MailTemplate\CommonService
- Repository:
    + App\Repositories\Eloquent\EmployeeRepository
    
**7. Table get data**

- mail_templates

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

### Save and send email
**1. URL**

- POST: {URL}/mail-templates/save-send

**2. Description**

- Save and send email.

**3. Last modify**

	// TO DO

**4. Parameter**

- title:
    + required
    + unique:mail_templates,title,NULL,id,company_id,$user->company_id 
    + max:255
- content:
    + required
- receiver_type:
    + required
    + in:MailTemplate::SEND_TO_EMPLOYEE,MailTemplate::SEND_TO_MANAGER
 
> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "Send email successfully."
        data:  {
            id
        }
    } 

- 422:
    {
        status_code: 422,
        message: "Data validation failed",
        errors:  []
    } 
        
- 500:
    {
        status_code: 500,
        message: "Send email fail.",
        errors:  []
    }    
```

**6. Code handle**

- Controller - function:
	+ MailTemplateController@saveAndSend
- Service:
    + App\Services\MailTemplate\SendMailTemplateService
    + App\Services\MailTemplate\CommonService
- Repository:
    + App\Repositories\Eloquent\MailTplateRepository
    
**7. Table get data**

- mail_templates

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

## Announcement
### List announcements
**1. URL**

- GET: {URL}/announcements

**2. Description**

- List announcements

**3. Last modify**

	// TO DO

**4. Parameter**

- per_page: nullable
- status: nullable
- sort_column: nullable
- sort_direction: nullable

> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "",
        data: {
            total
            per_page
            current_page
            last_page
            next_page_url
            prev_page_url
            from
            to
            items: [
                {
                    id
                    title
                    content
                    created_at
                    start_time_at
                    end_time_at
                    manager_id
                    company_id
                    is_emergency
                    is_send_all
                    type
                    manager
                    company: {
                        id
                        name
                    }
                }
            ]
        }
    }

- 500:
    {
        status_code: 500,
        message: "Occur an error while listing announcements.",
        errors: []
    }
```

**6. Code handle**

- Controller - function:
	+ AnnouncementController@index
- Service:
    + App\Services\AnnouncementService
- Repository:
    + App\Repositories\Eloquent\AnnouncementRepository
    
**7. Table get data**

- enigma_announcements

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

### Show announcement
**1. URL**

- GET: {URL}/announcements/{id}

**2. Description**

- Show announcement

**3. Last modify**

	// TO DO

**4. Parameter**

> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "",
        data: {
            id
            title
            content
            created_at
            start_time_at
            end_time_at
            manager_id
            company_id
            is_emergency
            is_send_all
            type
            manager
            company: {
                id
                name
            },
            announcement_branches: []
        }
    }

- 404:
    {
        status_code: 404,
        message: "Announcement is not found.",
        errors: []
    }
    
- 500:
    {
        status_code: 500,
        message: "Occur an error while showing announcement.",
        errors: []
    }
```

**6. Code handle**

- Controller - function:
	+ AnnouncementController@show
- Service:
    + App\Services\AnnouncementService
- Repository:
    + App\Repositories\Eloquent\AnnouncementRepository
    
**7. Table get data**

- enigma_announcements

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

### Create a announcement
**1. URL**

- POST: {URL}/announcements

**2. Description**

- Create a announcement

**3. Last modify**

	// TO DO

**4. Parameter**

- title:
    + required
    + max:255
- content:
    + required
- is_emergency: 
    + required
    + boolean
- start_time_at:
    + required
    + date_format:Y-m-d H:i
    + after:now
- end_time_at:
    + required
    + date_format:Y-m-d H:i
    + after:start_time_at
- is_send_all:
    + boolean
- branches

> **5. Response**

```php
- 201:
    {
        status_code: 201,
        message: "Successfully create announcement.",
        data: {
            id
        }
    }

- 422:
    {
        status_code: 422,
        message: "Data validation failed",
        errors: []
    }
        
- 500:
    {
        status_code: 500,
        message: "Occur an error while creating announcement.",
        errors: []
    }
```

**6. Code handle**

- Controller - function:
	+ AnnouncementController@store
- Service:
    + App\Services\AnnouncementService
- Repository:
    + App\Repositories\Eloquent\AnnouncementRepository
    
**7. Table get data**

- enigma_announcements

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

### Update announcement
**1. URL**

- PUT: {URL}/announcements/{id}

**2. Description**

- Update announcement

**3. Last modify**

	// TO DO

**4. Parameter**

- title:
    + required
    + max:255
- content:
    + required
- is_emergency: 
    + required
    + boolean
- start_time_at:
    + required
    + date_format:Y-m-d H:i
    + after:now
- end_time_at:
    + required
    + date_format:Y-m-d H:i
    + after:start_time_at
- is_send_all:
    + boolean
- branches

> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "Successfully update the announcement.",
        data: []
    }
    
- 422:
    {
        status_code: 422,
        message: "Data validation failed",
        errors: []
    }
    
- 500:
    {
        status_code: 500,
        message: "Occur an error while updating announcement.",
        errors: []
    }
```

**6. Code handle**

- Controller - function:
	+ AnnouncementController@update
- Service:
    + App\Services\AnnouncementService
- Repository:
    + App\Repositories\Eloquent\AnnouncementRepository
    
**7. Table get data**

- enigma_announcements

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

### Delete announcement
**1. URL**

- DELETE: {URL}/announcements/{id}

**2. Description**

- Delete announcement

**3. Last modify**

	// TO DO

**4. Parameter**

- id

> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "Successfully delete the announcement.",
        data: []
    }
    
- 422:
    {
        status_code: 422,
        message: "Data validation failed",
        errors: []
    }
    
- 500:
    {
        status_code: 500,
        message: "Occur an error while deleting announcement.",
        errors: []
    }
```

**6. Code handle**

- Controller - function:
	+ AnnouncementController@destroy
- Service:
    + App\Services\AnnouncementService
- Repository:
    + App\Repositories\Eloquent\AnnouncementRepository
    
**7. Table get data**

- enigma_announcements

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

## Employee Kintais
### List employee kintais
**1. URL**

- GET: {URL}/employee-kintais

**2. Description**

- List employee kintais

**3. Last modify**

	// TO DO

**4. Parameter**

- employee_name: nullable
- employee_name_kana: nullable
- branch_ids: nullable
- registration_date_from: nullable
- registration_date_to: nullable
- employee_code: nullable
- sort_column: nullable
- sort_direction: nullable
- per_page: nullable

> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "",
        data: {
            total
            per_page
            current_page
            last_page
            next_page_url
            prev_page_url
            from
            to
            items: [
            {
                id
                    branch_name
                    employee_code
                    employee_name
                    total_amount_of_salary
                    created_at
                    manager_name
                }
            ]
        }
    }

- 422:
    {
        status_code: 422,
        message: "Data validation failed",
        errors: []
    }

- 500:
    {
        status_code: 500,
        message: "Employee kintai cannot be listed.",
        errors: []
    }
```

**6. Code handle**

- Controller - function:
	+ EmployeeKintaiController@index
- Service:
- Repository:
    + App\Repositories\Eloquent\EmployeeKintaiRepository
    
**7. Table get data**

- employee_kintais

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO


## Payment request
### List branch payment requests
**1. URL**

- GET: {URL}/branch-payment-requests

**2. Description**

- List branch payment requests

**3. Last modify**

	// TO DO

**4. Parameter**

- schedule_id:
- sort_column: nullable
- sort_direction: nullable

> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "Login success",
        data: {
            [
                {
                    id
                    company_id
                    total_applies
                    total_employee_prepaid
                    total_amount_of_salary
                    total_transaction_fee
                    total_fee
                    amount_of_transfer_fee
                    total_consumption_tax
                    branch_code
                }
            ]
        }
    }

- 422:
    {
        status_code: 422,
        message: "Data validation failed",
        errors: []
    }

- 500:
    {
        status_code: 500,
        message: "Prepayment request cannot be listed.",
        errors: []
    }
```

**6. Code handle**

- Controller - function:
	+ BranchPaymentRequestController@index
- Service:
    + App\Services\BranchPaymentRequests\ListPaymentRequestService
    + App\Services\CommonApis\ScheduleSelectionService
- Repository:
    + App\Repositories\Eloquent\BranchPaymentRequestRepository
    
**7. Table get data**

- employee_payment_requests

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

### Show branch payment request
**1. URL**

- GET: {URL}/branch-payment-requests/{branchId}

**2. Description**

- Show branch payment request

**3. Last modify**

	// TO DO

**4. Parameter**

- employee_code
- employee_status
- employee_name
- employee_name_kana
- created_at_from
- created_at_to
- approved_at_from
- approved_at_to
- start_contract_time_from
- start_contract_time_to
- end_contract_time_from
- end_contract_time_to
- schedule_id
- sort_column
- sort_direction
- per_page

> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "",
        data: {
            total
            per_page
            current_page
            last_page
            next_page_url
            prev_page_url
            from
            to
            items: [
                {
                    id
                    employee_code
                    employee_name
                    total_apply
                    total_processing_processed
                    total_payback
                    total_error
                    amount_of_salary
                    total_transaction_fee
                    total_fee
                    total_consumption_tax
                    amount_of_transfer_fee
                }
            ]
        }
    }

- 403:
    {
        status_code: 403,
        message: "Permission denied.",
        errors: []
    }
    
- 422:
    {
        status_code: 422,
        message: "Data validation failed",
        errors: []
    }

- 500:
    {
        status_code: 500,
        message: "Employee payment request cannot be showed.",
        errors: []
    }
```

**6. Code handle**

- Controller - function:
	+ BranchPaymentRequestController@show
- Service:
    + App\Services\BranchPaymentRequests\ShowService
- Repository:
    + App\Repositories\Eloquent\BranchPaymentRequestRepository
    
**7. Table get data**

- employee_payment_requests

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

### Export csv of employee payment requests
**1. URL**

- GET: {URL}/employee-payment-requests/export-csv

**2. Description**

- Export csv employee payment request

**3. Last modify**

	// TO DO

**4. Parameter**

- branch_name
- bank_code
- branch_code
- employee_name
- employee_code
- status
- prepayment_apply_from
- prepayment_apply_to
- date_transfer_from
- date_transfer_to
- sort_column
- sort_direction

> **5. Response**

```php
- 200:
    Returns the number of characters read from the handle and passed through to the output (file csv with name have format employee-prepaid-salary-date('Ymd').csv)

- 500:
    {
        
        status_code: 500,
        message: "Employee payment request cannot be listed.",
        errors: []
    }
```

**6. Code handle**

- Controller - function:
	+ PaymentRequestController@exportCsv
- Service:
    + App\Services\PaymentRequestService
- Repository:
    + App\Repositories\Eloquent\EmployeePaymentRequestRepository
    
**7. Table get data**

- employee_payment_requests

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

### List employee payment request
**1. URL**

- GET: {URL}/employee-payment-requests

**2. Description**

- List employee payment request

**3. Last modify**

	// TO DO

**4. Parameter**

- branch_name: nullable
- bank_code: nullable
- branch_code: nullable
- employee_name
- employee_code: nullable
- status: nullable
- prepayment_apply_from: nullable
- prepayment_apply_to: nullable
- date_transfer_from: nullable
- date_transfer_to: nullable
- sort_column: nullable
- sort_direction: nullable
- per_page: nullable

> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "",
        data:  {
            total
            per_page
            current_page
            last_page
            next_page_url
            prev_page_url
            from
            to
            items: [
                {
                    id
                    payment_request_id
                    applied_at
                    status: {
                        id
                        name
                    },
                    branch_code
                    branch_name
                    employee_code
                    employee_name
                    amount_of_salary
                    amount_of_transfer_fee
                    transfer_date
                    amount_of_consumption_tax
                    total_fee
                    amount_of_transaction_fee
                    amount_of_fee
                    can_edit
                    status_object: {
                        id
                        name
                    }
                }
            ]
        }
    }

- 500:
    {
        status_code: 500,
        message: "Employee payment request cannot be listed.",
        errors: []
    }
```

**6. Code handle**

- Controller - function:
	+ PaymentRequestController@index
- Service:
    + App\Services\PaymentRequestService
- Repository:
    + App\Repositories\Eloquent\EmployeePaymentRequestRepository
    
**7. Table get data**

- employee_payment_request

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

## List activity log
**1. URL**

- GET: {URL}/activity-logs

**2. Description**

- List activity log

**3. Last modify**

	// TO DO

**4. Parameter**

- start_date: nullable
- end_date: nullable
- per_page: nullable

> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "",
        data: {
            item: [
                id
                company_id
                subject_id
                subject_type
                causer_id
                causer_type
                action
                action_id
                properties
                created_at
                causer_name
                company_name
            ]
        }
    }

- 500:
    {
        
        status_code: 500,
        message: "Occur an error while listing activity logs.",
        errors: []
    }
```

**6. Code handle**

- Controller - function:
	+ ActivityLogController@index
- Service:
- Repository:
    + App\Repositories\Eloquent\ActivityLogRepository
    
**7. Table get data**

- activity_logs

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO


## Google2FA
### Enable google2fa
**1. URL**

- POST: {URL}/2fa/enable

**2. Description**

- Enable google2fa

**3. Last modify**

	// TO DO

**4. Parameter**

> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "",
        data: {
            image
            secret_number
            secret_token
        }
    }

- 403:
    {
        status_code: 403,
        message: "ermission denied.",
        errors: []
    }

- 500:
    {
        status_code: 500,
        message: "You failed to activate dual protection.",
        errors: []
    }
```

**6. Code handle**

- Controller - function:
	+ Google2FAController@enableTwoFactor
- Service:
    + App\Services\Google2FAService
- Repository:
    
**7. Table get data**


**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO
	
### Disable google2fa
**1. URL**

- GET: {URL}/2fa/disable

**2. Description**

- Disable two factor

**3. Last modify**

	// TO DO

**4. Parameter**

> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "You succeeded in deactivating double protection.",
        data: {
            image
            secret_number
            secret_token
        }
    }

- 403:
    {
        status_code: 403,
        message: "ermission denied.",
        errors: []
    }

- 500:
    {
        status_code: 500,
        message: "You failed to deactivate heavy protection.",
        errors: []
    }
```

**6. Code handle**

- Controller - function:
	+ Google2FAController@disableTwoFactor
- Service:
    + App\Services\Google2FAService
- Repository:
    
**7. Table get data**


**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO
	
### Active google2fa
**1. URL**

- POST: {URL}/2fa/enable

**2. Description**

- Active two factor

**3. Last modify**

	// TO DO

**4. Parameter**

> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "You succeeded in activating dual protection.",
        data: []
    }

- 403:
    {
        status_code: 403,
        message: "Permission denied.",
        errors: []
    }
    
- 422:
    {
        status_code: 422,
        message: "Code number invalid",
        errors: []
    }

- 500:
    {
        status_code: 500,
        message: "You failed to activate dual protection.",
        errors: []
    }
```

**6. Code handle**

- Controller - function:
	+ Google2FAController@activeTwoFactor
- Service:
    + App\Services\Google2FAService
- Repository:
    
**7. Table get data**


**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO
	
	
## Employee lock
### List employee lock
**1. URL**

- GET: {URL}/employee-lockes

**2. Description**

- List employees who are locked

**3. Last modify**

	// TO DO

**4. Parameter**

- per_page: nullable

> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "",
        data: {
            total
            per_page
            current_page
            last_page
            next_page_url
            prev_page_url
            from
            to
            items: [
                {
                    id
                    code
                    name
                    name_kana
                    status
                    company_id
                    branch_id
                    created_at
                    last_login_failed
                    status_object: {
                        id
                        name
                    },
                    branch: {
                        id
                        code
                        name
                        name_kana
                    },
                    company: {
                        id
                        code
                        name
                    }
                }
            ]
        }
    }

- 500:
    {
        status_code: 500,
        message: "Employee cannot be listed.",
        errors: []
    }
```

**6. Code handle**

- Controller - function:
	+ EmployeeLockController@index
- Service:
    + App\Services\EmployeeLockService
- Repository:
    + App\Repositories\Eloquent\EmployeeRepository
    
**7. Table get data**

- employees

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

### Unlock employee
**1. URL**

- DELETE: {URL}/employee-lockes/{employee}/unlock

**2. Description**

- Unlock employee who are locked before

**3. Last modify**

	// TO DO

**4. Parameter**

> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "The employee has been unlocked successfully",
        data: []
    }

- 500:
    {
        status_code: 500,
        message: "The employee unlock failed",
        errors: []
    }
```

**6. Code handle**

- Controller - function:
	+ EmployeeLockController@unlockEmployee
- Service:
    + App\Services\EmployeeLockService
- Repository:
    + App\Repositories\Eloquent\EmployeeRepository
    
**7. Table get data**

- employees

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

## Password
### List show reset employees forgot password
**1. URL**

- GET: {URL}/password-managements

**2. Description**

- List employee forgot password by company

**3. Last modify**

	// TO DO

**4. Parameter**

- employee_code: nullable
- employee_name_kana: nullable
- employee_name: nullable
- apply_date_to: nullable
- apply_date_from: nullable
- sort_column: nullable
- sort_direction: nullable
- per_page: nullable

> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "",
        data: {
            total
            per_page
            current_page
            last_page
            next_page_url
            prev_page_url
            from
            to
            items: [
                {
                    id
                    employee_id
                    created_at
                    company_name
                    employee_code
                    employee_name
                    employee_name_kana
                    employee_phone
                    reset_at
                }
            ]
        }
    }

- 500:
    {
        status_code: 500,
        message: "Occur an error while listing employee forgot password.",
        errors: []
    }
```

**6. Code handle**

- Controller - function:
	+ ForgotPasswordController@listEmployeeForgotPassword
- Service:
- Repository:
    + App\Repositories\Eloquent\ForgotPasswordRepository
    
**7. Table get data**

- employees
- employee_forgot_passwords

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

### Show employee forgot password
**1. URL**

- GET: {URL}/password-managements/{id}

**2. Description**

- Show detail employee forgot password

**3. Last modify**

	// TO DO

**4. Parameter**

> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "",
        data: {
            updated_at
            employee_phone
            reset_at
        }
    }

- 500:
    {
        
        status_code: 500,
        message: "Employee cannot be showed.",
        errors: []
    }
```

**6. Code handle**

- Controller - function:
	+ ForgotPasswordController@show
- Service:
- Repository:
    + App\Repositories\Eloquent\ForgotPasswordRepository
    
**7. Table get data**

- employees
- employee_forgot_passwords

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

### Reset password
**1. URL**

- PUT: {URL}/password-managements/{id}

**2. Description**

- Reset employee password

**3. Last modify**

	// TO DO

**4. Parameter**

> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "Reset password success",
        data: []
    }

- 403:
    {
        status_code: 403,
        message: "Permission denied.",
        errors: []
    }
    
- 500:
    {
        status_code: 500,
        message: "Token is invalid",
        errors: []
    }
```

**6. Code handle**

- Controller - function:
	+ ForgotPasswordController@update
- Service:
- Repository:
    + App\Repositories\Eloquent\ForgotPasswordRepository
    + App\Repositories\Eloquent\EmployeeRepository
    
**7. Table get data**


**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

## Setting logs
### Get list setting log
**1. URL**

- POST: {URL}/setting-logs

**2. Description**

- List setting log

**3. Last modify**

	// TO DO

**4. Parameter**

- per_page: nullable
- sort_column: nullable
- sort_direction: nullable

> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "",
        data: {
            total
            per_page
            current_page
            last_page
            next_page_url
            prev_page_url
            from
            to
            items: [
                {
                    created_at
                    causer_name
                    branch_name
                    item_name
                    value_before
                    value_after
                }
            ]
        }
    }

- 401:
    {
        status_code: 401,
        message: "Your login information was incorrect.",
        errors: []
    }

- 500:
    {
        status_code: 500,
        message: "Setting log cannot be listed.",
        errors: []
    }
```

**6. Code handle**

- Controller - function:
	+ SettingLogController@index
- Service:
    + App\Services\SettingLogService
- Repository:
    + App\Repositories\Eloquent\SettingLogRepository
    
**7. Table get data**

- setting_logs
- setting_log_details

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO

### Get csv setting log
**1. URL**

- GET: {URL}/setting-logs/csv

**2. Description**

- Get csv setting log

**3. Last modify**

	// TO DO

**4. Parameter**

- sort_column: nullable
- sort_direction: nullable

> **5. Response**

```php
- 200:
    {
        status_code: 200,
        message: "",
        data: []
    }

- 500:
    {
        status_code: 500,
        message: "Setting log cannot be listed.",
        errors: []
    }
```

**6. Code handle**

- Controller - function:
	+ SettingLogController@getSettingLogCsv
- Service:
    + App\Services\SettingLogService
- Repository:
    + App\Repositories\Eloquent\SettingLogRepository
    
**7. Table get data**

- setting_logs
- setting_log_details 

**8. Table change data(insert, update, delete)**

	// TO DO

**9. SQL**
	
	// TO DO
