## 1. Create schedule
### 1.1 Giới thiệu
**Mục đích**: Tạo mới một schedule cho company.

Một schedule là khoảng thời gian bắt đầu từ thời gian đóng kintai của tháng trước(lần đóng kintai gần nhất trong quá khứ) đến thời gian đóng kintai sắp đến(thời gian gần nhất trong tương lai).

Một công ty cần tạo schedule khi có thời gian đóng kintai trước nhưng chưa có thời gian đóng kintai sau(không có thời gian đóng kintai trong tương lai).

Một schedule gồm các thông tin chính: thời gian đóng kintai, thời gian có thể ứng tiền, thời gian kết thúc ứng tiền, thời gian trả lương, thời gian trả phí.

**Ghi chú**: Sau khi tạo được schedule cho company thì tạo schedule cho từng branch(vì có những setting chỉ riêng cho branch nên cần tạo schedule company và schedule branch tương ứng với nhau)

### 1.2 Command
``` 
$ cd /var/www/enigma-company-batch/current/ & php artisan schedule:create-company-payment
```

**Parameters**: Không.

### 1.3 Time
Hệ thống hiện tại có 2 batch song song, một batch chính và một batch dự phòng. Batch dự phòng chạy khi batch chính không chạy(không update schedule_settings và batch dự phòng ping không được với batch chính)

--- **Batch chính**: 0:00:00 mỗi ngày

--- **Batch dự phòng**: 0:00:05 mỗi ngày

### 1.4 Specific
**Lấy thông tin của company cần tạo mới schedule.**

Dựa vào thời gian hiện tại, và thời gian đóng kintai của company, lấy thông tin những công ty đã kết thúc schedule cũ mà chưa có schedule mới.

--- Thông tin được lấy ra gồm: các setting của cycle trong company_payment_cycles, một vài setting(company_payment_schedules) của cycle cũ(id, cycle_number, end_applying_time, closing_kintai_time).

**Xử lý tạo schedule mới cho từng công ty.**

Dựa vào thông tin các company đã lấy được ở trên sau đó xử lý cho từng company:

--- Tạo schedule mới cho công ty(company_payment_schedules) dựa trên những thông tin lấy được(thông tin cycle và thông tin schedule cũ)

--- Update thông tin của employee của các công ty vừa mới tạo schedule(một số thông tin cá nhân như phí bảo hiểm cần update, các setting này sẽ được giữ nguyên suốt một schedule và được update theo setting mới lúc schedule mới được tạo ra.)

--- Xóa thông tin trong bảng previous_branch_schedules(những employee có chỉnh sửa branch thì lúc sử dụng vẫn lấy những setting của schedule branch củ, chỉ khi nào tạo mới schedule thì mới app dụng branch mới.)

**Log result:**

Log lại các thông tin liên quan đến lần chạy batch(kết quả thành công hay thất bại, nội dung thành công hay thất bại...)

### 1.5 Affer create schedule.
Sau khi tạo schedule cho compay thì tạo [schedule cho branch](#5-create-branch-schedule) của các copany này.

## 2. Create branch schedule
### 2.1 Giới thiệu
**Mục đích**: Tạo mới một schedule cho branch của các company đã update schedule.

Mỗi công ty sẽ có nhiều branch, mỗi branch sẽ có một setting riêng cho branch đó. Cũng giống như schedule của company, branch cũng có schedule riêng.

Setting schedule của branch phụ thuộc vào setting schedule của company.

**Thời gian chạy batch**: Sau khi hoàn thành create schedule(create schedule của company).

**Ghi chú**:

Setting riêng của branch:

--- upper_limit_rate: tỉ lệ quy đổi tối đa khi up kintai(ví dụ tỉ lệ 90%, up kintai cho employee: lương 1000 thì employee có thể ứng tối đa 1000*0.9 = 900)

--- insurance_fee: phí bảo hiểm(nếu employee không setting phí bảo hiểm riêng thì sẽ lấy theo phí bảo hiểm của branch, nếu branch không setting thì lấy theo company.)

### 2.2 Command
```
 $ cd /var/www/enigma-company-batch/current/ & php artisan schedule:branch_prepaid
```

**Parameters**: Không.

### 2.3 Time

Hệ thống hiện tại có 2 batch song song, một batch chính và một batch dự phòng. Batch dự phòng chạy khi batch chính không chạy(không update schedule_settings và batch dự phòng ping không được với batch chính)

--- **Batch chính**: Sau khi batch chính hoàn thành create schedule cho company(0:00:00 mỗi ngày) thì chạy create schedule cho branch.

--- **Batch dự phòng**: Sau khi batch chính hoàn thành create schedule cho company(0:00:05 mỗi ngày) thì chạy create schedule cho branch.

### 2.4 Specific

**Lấy thông tin của branch cần tạo mới schedule.**

Dựa vào sự thay đổi trong schedule của company(company_payment_schedules) để lấy những branch tương ứng cần tạo schedule.

**Xử lý tạo schedule mới cho từng công ty.**

Dựa vào thông tin các branch đã lấy được ở trên sau đó xử lý cho từng branch:

--- Tạo schedule mới cho branch(branch_prepaid_schedules) dựa trên những thông tin lấy được(thông tin branch_prepaid_setting và thông tin company_payment_schedules)

**Log result:**

Log lại các thông tin liên quan đến lần chạy batch(kết quả thành công hay thất bại, nội dung thành công hay thất bại...)

## 3. Send Notification
### 3.1 Giới thiệu

**Mục đích**

- Gửi thông báo khi cập nhật employee kintai thành công(chỉ gửi thông báo cho những nhân viên được cập nhật kintai).

 + Cập nhật kintai bằng import csv kintai tại repo company_backend(đối với tất cả công ty).

 + Cập nhật kintai bằng csv(upload to sftp server) trong cronjob cho tất cả công ty.

- Gửi thông báo theo lịch trình: thông báo được tạo ở repo admin có ngày gửi nhỏ hơn hoặc bằng ngày hiện tại(gửi cho tất cả nhân viên của công ty được chỉ định).

- Hiện đang gởi notification tới mobile bằng Urban Airship(https://docs.urbanairship.com/api/ua/#push-api).

### 3.2 Command

 ```$  cd /var/www/enigma-company-batch/current/ & php artisan notification:send
 ```

**Parameters**: Không.

### 3.3 Time

 Hệ thống hiện tại có 2 batch song song, một batch chính và một batch dự phòng. Batch dự phòng chạy khi batch chính không chạy(không update schedule_settings và batch dự phòng ping không được với batch chính)

--- **Batch chính**: mỗi phút một lần

--- **Batch dự phòng**: không

### 3.4 Specific

**Điều kiện gửi thông báo:**

- Lấy dữ liệu cho thông báo cập nhật kintai thỏa điều kiện(chỉ gửi cho những nhân viên được cập nhật kintai):

 + Chỉ lấy những nhân viên được cập nhật kintai(nằm trong notification_recevice) có trạng thái đang sử dụng(Employee::STATUS_USING) hoặc ngừng hoạt động(Employee::STATUS_STOPPED) nhưng có ngày ngừng hoạt động sau ngày hiện tại.

 + Device token của nhân viên phải tồn tại.

 + Lấy những thông báo đang gửi(status = Notification::SEND_SENDING), có ngày gửi <= ngày hiện tại và không cho phép gửi đến tất cả nhân viên(is_send_all = false).

 + target_type = TYPE_COMPANY

- Lấy dữ liệu cho thông báo được tạo ở repo admin thỏa điều kiện(gửi cho tất cả nhân viên của công ty được chỉ định):

 + Chỉ lấy những nhân viên đang sử dụng(Employee::STATUS_USING) hoặc ngừng hoạt động(Employee::STATUS_STOPPED) mà có ngày ngừng hoạt động sau ngày hiện tại.

 + Device token của nhân viên phải tồn tại.

 + Lấy những thông báo đang được xử lý(status = Notification::SEND_PROCESSING), có ngày gửi nhỏ hơn hoặc bằng ngày hiện tại. Nếu thông báo có trạng thái is_send_all = false thì chỉ lấy thông tin những nhân viên được nhận thông báo(nằm trong employee_notification), nếu is_send_all = true thì lấy thông tin của tất cả nhân viên.

 **Chú ý:** Sau khi gửi thành công, insert dữ liệu thông tin nhân viên vào notification_receives.

**Nội dung notification lấy từ DB:**

Data có dạng: id, title(tiêu đề thông báo), content(nội dung thông báo), send_data(ngày gửi thông báo), device_token, device_type(loại thiết bị ios hoặc android).

**Update trạng thái thông báo:**

Cập nhật trạng thái cho những thông báo được gửi đi (được xử lý(Notification::SEND_PROCESSED) nếu gửi thành công hoặc fail(Notification::SEND_ERROR) nếu gửi thấy bại).

**Log result:**

Log lại các thông tin liên quan đến lần chạy batch(kết quả thành công hay thất bại, nội dung thành công hay thất bại...).

## 4. Create maintenance time for bank
### 4.1 Giới thiệu
**Mục đích:**

 Tạo thời gian maintain cụ thể mỗi tháng cho ngân hàng.

### 4.2 Command
```
 $ cd /var/www/enigma-company-batch/current/ & php artisan maintenance:create
```

**Parameters:** Không.

### 4.3 Time
Hệ thống hiện tại có 2 batch song song, một batch chính và một batch dự phòng. Batch dự phòng chạy khi batch chính không chạy(không update schedule_settings và batch dự phòng ping không được với batch chính)

--- **Batch chính**: 0h ngày đầu tiên của mỗi tháng

--- **Batch dự phòng**: 0h5 ngày đầu tiên của mỗi tháng

### 4.4 Specific 
- Lấy thời gian maintain trong bảng enigma setting

+ Xử lý tạo thời gian bắt đầu, kết thúc cụ thể insert vào bảng maintenance_settings

- Log result:

+ Log lại các thông tin liên quan đến lần chạy batch(kết quả thành công hay thất bại, nội dung thành công hay thất bại...)

## 5. Import kintai

### 5.1 Giới thiệu

- Đây là chức năng tự động import kintai. Thay vì phải đăng nhập vào màn hình quản trị của company để import bằng tay thì hệ thống hỗ trợ chức năng import kintai tự động.

- Khách hàng cần tạo file có tên bắt buộc là Attendance.csv (nội dung của file là thông tin tiền lương của nhân viên) và đưa file lên server bằng Ftp tool..

### 5.2 Command
`$ cd /var/www/enigma-company-batch/current/ & php artisan ftp:import-kintai`

### 5.3 Thời gian chạy

- Hệ thống hiện tại có 2 batch song song, một batch chính và một batch dự phòng. Batch dự phòng chạy khi batch chính không chạy.

 + Batch chính: 8h mỗi ngày.

 + Batch dự phòng: 8h30 mỗi ngày.

### 5.4 Flow xử lý

**Khách hàng tạo 1 file có tên bắt buộc là Attendance.csv chứa thông tin về lương của nhân viên và đưa file lên server bằng Ftp tool.**

- Header của file Attendance.csv sẽ được setting ở màn hình quản trị enigma-admin.

**Hệ thống sẽ tự động đưa file lên S3 với thời gian 5 phút một lần.**

- File sẽ đặt trong work folder trong folder của mỗi công ty.

**Hệ thống kiểm tra Schedule từ table: schedule_settings.**

**Hệ thống sẽ tiến hành xử lý import kintai data.**

- Scan trong work folder của mỗi công ty, trường hợp file CSV tồn tại thì thực hiện xử lí import tất cả file CSV.

- Data sẽ được lưu vào table employee_kintais, employee_kintai_values, employee_salaries.

- Đối với trường hợp duplicate code thì update status import và row erros đó.

- Đối với file kintai có chứa dữ liệu data bị error thì mình vẫn cho import được những data không bị error. Đối với việc import loại file này sẽ để status là "警告終了/warning completed" và để thêm "警告詳細/warning detail" để bít hàng nào có error và tại sao lại bị error.

- Đối với file kintai có chứa dữ liệu data mà tất cả các row error thì đối với việc import loại file này sẽ để status là failed.

- Khi hoàn thành xử lí import, nếu xử lí kết thúc normal thì sẽ di chuyển file đối tượng sang folder "imported".

+ trong trường hợp di chuyển file thì sẽ thay đổi trên dựa trên rule sau::

=> File name trước khi thay đổi: [File name].csv

=> File name sau khi thay đổi: [File name]_[Ngày giờ hoàn thành xử lí import(theo format yyyymmdd)].csv

- Khi hoàn thành xử lí import, trường hợp xử lí hoàn thành kiểu abnormal thì sẽ di chuyển đến folder "error".

 + Trường hợp di chuyển file thì sẽ đổi tên file dựa trên rule dưới đây:

  => File name trước khi thay đổi: [File name].csv

  => File name sau khi thay đổi: [File name]_[Ngày giờ hoàn thành xử lí import(theo format yyyymmdd)].csv

- Trường hợp ko có CSV file nào trong folder "work" của mỗi công ty thì sẽ kết thúc mà ko thực hiện xử lí gì cả.

- Sau khi xử lí import được thực hiện thì sẽ ko còn file trong folder "work".

- Khi hoàn thành xử lí import, trường hợp xử lí hoàn thành kiểu abnormal (import failed) thì sẽ thông báo error bằng mail đến:

 => ENIGMA PRODUCT MEMBER

 => MEMBER OPERATE ENIGMA

 => COMPANY ADMIN

- Trường hợp import có status Warning completed thì không gởi mail thông báo.

- Trường hợp import kintai thành công thì có gửi notification cho employee đã được update kintai.


**Mail**

- Gửi mail thông báo lỗi đối với trường hợp import failed.

- Không gửi mail thông báo lỗi đối với trường hợp import warning error.


**Log result**

- Log lại các thông tin liên quan đến lần chạy batch (kết quả thành công hay thất bại, file có bao nhiêu record).

- Trường hợp import lỗi hệ thống sẽ lưu log ở table: schedule_logs

## 6. Import employee

### 6.1 Giới thiệu

- Đây là chức năng tự động import employee. Thay vì phải đăng nhập vào màn hình quản trị của company để import bằng tay thì hệ thống hỗ trợ chức năng import tự động.

- Khách hàng cần tạo file có tên bắt buộc là Employee.csv (nội dung của file là danh sách nhân viên bao gồm thông tin nhân viên để thêm mới hoặc cập nhật thông tin nhân viên trong hệ thống) và đưa file lên server bằng Ftp tool.

### 6.2 Command.

`$ cd /var/www/enigma-company-batch/current/ & php artisan ftp:import-employee`

### 6.3 Thời gian chạy

- Hệ thống hiện tại có 2 batch song song, một batch chính và một batch dự phòng. Batch dự phòng chạy khi batch chính không chạy.

 + Batch chính: 8h mỗi ngày

 + Batch dự phòng: 8h30 mỗi ngày

### 6.4 Flow xử lý

**Khách hàng tạo 1 file có tên bắt buộc là Employee.csv chứa thông tin nhân viên và đưa file lên server bằng Ftp tool.**

- Nội dung file là danh sách nhân viên bao gồm thông tin để thêm mới hoặc cập nhật lại.

- Header mặc định của file Employee.csv

Header (English) | Header (Japan)
--- | ---
code | ｽﾀｯﾌ番号 |
branch_name | 事業所名 |
branch_code | 本支店コード |
email | メールアドレス |
name | 氏名(漢字) |
name_kana | 氏名(カナ) |
start_contract_time | 初回稼働日 |
contract_type | 契約種別(従業員属性) |
end_contract_time | 退社日 |
is_social_insurance | 社会保険加入状況 |
insurance_policy | 社会保険控除額設定 |
insurance_fee | 社会保険控除金額 |
is_employment_insurance | 雇用保険加入状況 |
month_salary | 月給 |
ot_hour_salary | 残業代(時間単位) |
hour_salary | 時給 |
daily_salary | 日給 |
bank_code | 銀行番号 |
bank_name_kana | 銀行名 |
bank_branch_code | 支店番号 |
bank_branch_name_kana | 支店名 |
deposit_type | 区分 |
bank_account_number | 口座番号 |
bank_account_holder_kana | 口座名義人 |
resignation | 退社 |
payment_type | 支払種別 |
welfare_pension_class | 厚生年金等級 |
health_insurance_class | 健康保険等級 |
welfare_pension_acquisition_date | 厚生年金取得日 |
welfare_pension_loss_date | 厚生年金喪失日 |
employement_insurance_acquisition_date | 雇用保険-取得日 |
employement_insurance_loss_date | 雇用保険-喪失日 |

**Hệ thống sẽ tự động đưa file lên S3 với thời gian 5 phút một lần.**

- File sẽ đặt trong work folder trong ftp folder của công ty đã đăng ký ftp.

 + Trường hợp kết thúc normal thì file sẽ được lưu vào imported folder.

 + Còn trường hợp kết thúc abnormal thì file sẽ được lưu vào error folder.

**Hệ thống kiểm tra Schedule từ table: schedule_settings.**

**Hệ thống sẽ tiến hành xử lý import employee data.**

- Với những nhân viên (có trạng thái đang sử dụng hoặc tạm ngừng hoạt động) có branch hiện tại khác branch trong file import thì hệ thống sẽ cập nhật branch hiện tại thành branch schedule trước. 

- Với những nhân viên chưa tồn tại thì hệ thống tạo mới thông tin cho nhân viên với status "approved service". 

- Với những nhân viên đã tồn tại thì hệ thống sẽ cập nhật thông tin cho nhân viên. 

- Sau khi import hoàn thành thì file Employee.csv sẽ bị xóa.

**Mail**

- Gửi mail thông báo lỗi đối với import failed.

- Không gửi mail thông báo lỗi đối với import warning error.

**Log result:**

- Log lại các thông tin liên quan đến lần chạy batch (kết quả thành công hay thất bại, file có bao nhiêu record)

- Trường hợp import lỗi hệ thống sẽ lưu log ở table: schedule_logs